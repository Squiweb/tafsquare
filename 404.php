<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>
  <header class="error_header">

    <div class="bg">
      <img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
    </div>

    <div class="container verticalaligna">
      <div class="row row-centered">
        <div class="col-md-12 col-centered">
					<img src="img/404.png" alt="Erreur 404">
					<h3>La page demandée n’est pas disponible sur ce serveur.</h3>
        </div>
      </div>
    </div>

  </header>
<?php include 'includes/footer.php' ?>
