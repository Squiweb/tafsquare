
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>
<header class="small_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container verticalalign">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1 class="yellow">Conditions générales</h1>
			</div>
		</div>
	</div>

</header>


<div class="container all_cgu">
	<div class="row">
		<h3>CONDITIONS GENERALES D'UTILISATION DU SITE WEB : " http://www.tafsquare.com "</h3>

		<p><strong>1.- Le site web est administré et géré par Tafsquare située (adresse complète de Tafsquare) qui peut être contacté au (téléphone de Tafsquare) et à l'adresse email : (email de contact avec Tafsquare, de contact « client »), inscrit(e) à la BCE sous le numéro : (numéro BCE de Tafsquare).</strong></p>
		<p>« Le site » est le site web http://www.tafsquare.com</p>
		<p>« Le Travail à Faire », dénommé « TAF » est toute prestation de services commandée via le site par un client et pouvant être fournie par un professionnel.</p>
		<p>« Le professionnel » est l’utilisateur inscrit sur le site qui répond à un TAF afin de le réaliser et agit à des fins qui entrent dans le cadre de son activité commerciale, industrielle, artisanale, ou libérale.</p>
		<p>« L’utilisateur » est le professionnel, toute personne physique ou morale se rendant simplement sur le site de manière occasionnelle, ou toute personne physique ou morale qui possède un numéro TVA ou un numéro d’entreprise, inscrite sur le site, qui utilise le site, et crée ou répond à un TAF sur le site.</p>
		<p>L’utilisateur déclare avoir reçu de Tafsquare toutes les informations et tous les conseils nécessaires pour exploiter correctement le site web (ci-après dénommé "tafsquare") accessible à l’adresse http://www.tafsquare.com.</p>
		<p>Afin d’accéder à la plate-forme, l’utilisateur s’inscrira sur le site et créera un nom d’utilisateur (login) et un mot de passe. L’utilisateur reste responsable de la confidentialité de son mot de passe et de toute utilisation qui pourrait survenir à son insu. En cas de doute sur le caractère confidentiel du mot de passe, il appartient à l’utilisateur d’en aviser immédiatement et par écrit Tafs- quare à l’adresse de courriel : (adresse de contact avec Tafsquare).</p>
		<p>Il appartient à l’utilisateur de se doter des moyens humains et informatiques nécessaires afin d’assurer sa connectivité avec la plate-forme. Ainsi, l’utilisateur reste seul responsable des moyens informatiques et humains qu’il jugera utile de mettre en œuvre de ce fait.</p>
		<p>L’accès à la plate-forme n’est permis que sous réserve de l’acceptation des présentes conditions d'utilisation, des conditions générales de prestation de services, et de la charte vie privée, que Tafsquare sera libre de modifier à tout moment en plaçant directement sur la plate-forme des notifications de changement ou des liens vers de telles notifications.</p>
		<p>Toute notification pourra également être adressée à l’utilisateur par e-mail ou par courrier postal.</p>
		<p>Les règles et lignes de conduite supplémentaires seront réputées faire partie intégrante des présentes conditions générales. Il est donc conseillé à l’utilisateur de se référer régulièrement à la dernière version des présentes conditions disponible en permanence à l'adresse suivante : (lien où l’ensemble des conditions générales (utilisation site web, prestation de services, et charte vie privée, seront disponibles)).</p>
		<p>En accédant à la plate-forme, l’utilisateur accepte de fournir des informations exactes, à jour et complètes et d’en assurer la mise à jour régulière. Dans le cas contraire, Tafsquare sera en droit de suspendre ou de résilier le compte, ou de refuser l'accès à tout ou partie de la plate-forme. Tafsquare n’est en aucun cas responsable d’un manquement dans ce domaine.</p>
		<p><strong>2.- Dans le cadre de l’utilisation de la plate-forme, l’utilisateur s’interdit expressément de :</strong></p>
		<ul>
			<li>afficher, télécharger, envoyer, transmettre par e-mail ou de toute autre manière tout contenu qui soit illégal, nuisible, menaçant, abusif, constitutif de harcèlement, diffamatoire,
vulgaire, obscène, menaçant pour la vie privée d'autrui, haineux, raciste, ou autrement répréhensible ;</li>
			<li>consulter, afficher, télécharger, envoyer, transmettre, tout contenu qui serait contraire aux lois internationales en vigueur ;</li>
			<li>tenter d'induire en erreur d'autres utilisateurs en usurpant le nom ou la dénomination sociale d'autres personnes ;</li>
			<li>télécharger, afficher, envoyer, transmettre par e-mail ou de toute autre manière tout contenu violant tout brevet, marque déposée, secret de fabrication, droit de propriété intel-
lectuelle ou tout autre droit de propriété appartenant à autrui ;</li>
			<li>télécharger, afficher, envoyer, transmettre par e-mail ou de toute autre manière toute publicité ou tout matériel promotionnel non sollicités ou non autorisés (notamment se livrer
à du « spam », à la transmission de « junk mail », de chaîne de lettres ou toute autre forme de sollicitation) ;</li>
			<li>télécharger, afficher, transmettre par e-mail ou de toute autre manière tout contenu comprenant des virus informatiques ou tout autre code, dossier ou programme conçus pour
interrompre, détruire ou limiter la fonctionnalité de tout logiciel, ordinateur, ou outil de télécommunication sans que cette énumération ne soit limitative ;</li>
			<li>commettre toute action ayant un effet perturbateur entravant la capacité des autres utilisateurs d’accéder à la plate-forme ;</li>
			<li>entraver ou perturber le service, les serveurs, les réseaux connectés au service, ou refuser de se conformer aux conditions requises, aux procédures, aux règles générales ou aux
dispositions réglementaires applicables aux réseaux connectés à la plate-forme ;</li>
			<li>harceler de quelque manière que ce soit un autre ou plusieurs autres utilisateurs ;</li>
			<li>collecter et stocker des données personnelles afférentes aux autres utilisateurs.</li>
		</ul>
		<p><strong>3.- Tafsquare garantit que les éléments, services et fonctionnalités mis à la disposition de l’utilisateur, s’ils sont utilisés conformément aux indications données, sont substantiellement conformes aux standards généralement admis, et que les logiciels et tous les éléments créés par Tafsquare et mis à la disposition de l’utilisateur respectent les droits des tiers, et de façon générale ne sont pas illicites.</strong></p>
		<p>Tafsquare est tenu, de manière générale, à une obligation de moyen. Tafsquare ne sera en aucun cas tenu responsable des éventuels dommages directs ou indirects encourus par l’utilisateur dans le cadre de l’utilisation de la plate-forme et/ou de son contenu mis à sa disposition. De même, Tafsquare n’est tenu que de son dol ou sa faute lourde. Il n’est pas responsable du dol ou de la faute lourde de ses préposés, commettants et en règle générale de ses agents d’exécution.</p>
		<p>L’utilisateur tiendra également Tafsquare indemne de toute réclamation, dans l'un ou l'autre des cas suivants :</p>
		<ul>
			<li>perte d'occasions ou de revenus d'affaires reliés au fonctionnement ou à l'absence de fonctionnement, ou à l'utilisation ou à l'absence d'utilisation de la plate-forme, ou du conte- nu s'y trouvant ou devant s'y trouver ;</li>
			<li>intrusion illégale ou non autorisée de tout tiers dans le serveur web ou la plate-forme de Tafsquare ;</li>
			<li>introduction d'un virus informatique dans le serveur web ou sur la plate-forme de Tafsquare ;</li>
			<li>encombrement temporaire de la bande passante ;</li>
			<li>interruption du service de connexion Internet pour une cause hors du contrôle de Tafsquare.</li>
		</ul>
		<p>L’utilisateur accepte que les fonctionnalités proposées au travers de la plate-forme sont susceptibles d’évoluer. Ainsi, certaines seront supprimées et d’autres ajoutées, sans que l’utilisateur ne puisse considérer que l’accès à une fonctionnalité particulière ne constitue un droit acquis. De même, Tafsquare décidera seul de l’opportunité d’inclure et/ou de supprimer tout contenu pré- senté sur la plate-forme.</p>
		<p><strong>4.- Tafsquare se réserve le droit, à tout moment et pour quelque motif que ce soit, de modifier ou d’interrompre temporairement ou de manière permanente tout ou partie de l’accès à la plate-forme et ce sans devoir en informer préalablement l’utilisateur.</strong></p>
		<p>Il en sera ainsi par exemple en cas de maintenance de la plate-forme ou de modification importante des services et/ou des fonctionnalités proposés. Tafsquare ne pourra être tenu respon- sable pour tout dommage direct ou indirect lié à une modification, suspension ou interruption de l’accès à la plate-forme, pour quelque cause que ce soit.</p>
		<p><strong>5.- L’utilisateur bénéficie d’une licence d’utilisation de la plate-forme et de son contenu, incessible et exclusivement limitée à une utilisation à des fins personnelles ou professionnelles. La durée de la licence d’utilisation est limitée à la durée de validité du nom d’utilisateur et du mot de passe.</strong></p>
		<p>Les informations, logos, dessins, marques, modèles, slogans, chartes graphiques, etc. accessibles au travers de la plate-forme sont protégés par le droit national et international de la propriété intellectuelle et/ou industrielle.</p>
		<p>Sauf s'il a expressément été autorisé à cet effet par Tafsquare et/ou le tiers concerné, l’utilisateur n’est pas autorisé à modifier, reproduire, louer, emprunter, vendre, distribuer ou créer d’œuvres dérivées basées en tout ou en partie sur les éléments présents sur la plate-forme. Il est par conséquent interdit (et l’utilisateur ne peut accorder à autrui l'autorisation) de copier, modifier, créer une œuvre dérivée, inverser la conception ou l'assemblage ou de toute autre manière tenter de trouver le code source, vendre, attribuer, sous-licencier ou transférer de quelque manière que se soit tout droit afférent à la plate-forme et à son contenu.</p>
		<p><strong>6.- L’éventuelle illégalité ou nullité d’un article, d’un paragraphe ou d’une disposition (ou partie d’un article, d’un paragraphe ou d’une disposition) ne saurait affecter de quelque manière la légalité des autres articles, paragraphes ou dispositions de ces conditions générales, ni non plus le reste de cet article, de ce paragraphe ou de cette disposition, à moins d’intention contraire évidente dans le texte.</strong></p>
		<p><strong>7.- Les titres utilisés dans les présentes conditions générales ne le sont qu’à des fins de référence et de commodité seulement. Ils n’affectent en rien la signification ou la portée des disposi- tions qu’ils désignent.</strong></p>
		<p><strong>8.- Les présentes conditions d’utilisation du site ainsi que les conditions générales de prestation de service représentent la totalité et l’intégralité de l’entente intervenue entre les parties. Aucune déclaration, représentation, promesse ou condition non contenue dans les présentes conditions générales ne peut et ne doit être admise pour contredire, modifier ou affecter de quelque façon que ce soit les termes de celles-ci.</strong></p>
		<p>En outre, les présentes conditions générales ainsi que les conditions générales de prestation de service se substituent à tout accord éventuellement intervenu antérieurement entre les parties et s’appliquent à tout nouvel accord.</p>
		<p><strong>9.- Toute contestation relative aux services fournis par Tafsquare, de même que toute contestation quant à la validité, l’interprétation ou l’exécution des présentes conditions générales sera soumise au droit belge et de la compétence exclusive des tribunaux de l’arrondissement judiciaire du Hainaut.</strong></p>

		<h3>CONDITIONS GENERALES DE PRESTATION DE SERVICES<br>Définitions et champ d’application</h3>

		<p>Les présentes conditions générales forment le contrat liant les parties, à savoir Tafsquare, situé(e) (adresse complète), inscrit(e) à la BCE sous le numéro : (numéro BCE de Tafsquare), et le client.</p>
		<p>« Le site » est le site web http://www.tafsquare.com</p>
		<p>«Le client» est toute personne physique ou morale qui possède un numéro TVA ou un numéro d’entreprise, et qui utilise le site et crée ou répond à un TAF sur le site.</p>
		<p>«Le professionnel» est le client qui répond à un TAF afin de le réaliser et agit à des fins qui entrent dans le cadre de son activité commerciale, industrielle, artisanale ou libérale.</p>
		<p>« Le Travail à Faire, ci-après dénommé « TAF » est toute prestation de services commandée via le site par un client et pouvant être fournie par un professionnel.</p>
		<p>Les présentes conditions générales sont accessibles sur le site de Tafsquare, de sorte qu’en s’inscrivant sur le site et en passant commande pour, ou en répondant à, un TAF, le client confirme son acceptation des présentes conditions générales.</p>
		<p>Tafsquare se réserve le droit de modifier les présentes conditions générales à tout moment sans notification préalable. Ces modifications s'appliqueront à toutes les commandes de services, créations de TAF, et réponses à des TAF, passés ultérieurement.</p>
		<p>Inscription, Prix et Commande d’un TAF</p>
		<p>En s’inscrivant sur le site, et pendant toute la durée de son inscription, le client autorise expressément Tafsquare à utiliser toute adresse mail que le client aura renseigné afin de lui adresser des mails l’informant des produits et/ou des services proposés par Tafsquare.</p>
		<p>Le prix des services est indiqué en euros, hors TVA. Pour soumettre un TAF, le client documente l’ensemble des informations qui lui sont demandées, s’inscrit sur le site ou s'authentifie et ensuite l'envoie aux professionnels sélectionnés sur base de leur localisation géographique. Il n’effectuera le paiement qu'à la condition qu'un professionnel au moins ait montré de l'intérêt au TAF qu'il a soumis. Ce n'est qu'après avoir payé qu'il pourra visualiser les informations détaillées du ou des professionnels ayant montré de l'intérêt pour son TAF.</p>
		<p>Le client recevra ensuite un email de Tafsquare validant le TAF, reprenant un récapitulatif du TAF qu’il a créé, et notamment le métier, les dates de début et de fin, l’adresse, le titre, la descrip- tion, ainsi que le budget du TAF.</p>
		<p>Lorsque Tafsquare ne valide pas le TAF, un email expliquant les raisons de ce refus est envoyé au client.</p>
		<p>Tafsquare se réserve le droit de suspendre ou de refuser la création d’un TAF, notamment dans le cas où les données communiquées par le client s’avèrent manifestement erronées ou incom- plètes, ou lorsque le TAF créé par le client n’est, à la seule discrétion de Tafsquare, pas licite, conforme aux règles habituelles de bonnes vies et mœurs, ou de manière générale conforme à une destination normale et aux finalités du site.</p>
		<p>Plus spécifiquement, Tafsquare se réserve le droit de suspendre ou de refuser la création d’un TAF lorsqu’un numéro de téléphone est écrit dans le titre ou la description du TAF, lorsque le montant proposé pour le TAF n’est pas réaliste eu égard aux usages habituels, lorsque les dates proposées pour le TAF ne sont pas cohérentes, ou encore lorsque le TAF n’a pas de rapport avec le métier sélectionné pour la création du TAF.</p>
		<p>Lorsque le TAF a été créé, il est ensuite transmis par email aux professionnels sélectionnés d’après leur localication. Ces professionnels pourront se montrer intéressés par le TAF pendant une période de 14 jours.</p>
		<p>Si aucun professionnel ne s’est montré intéressé endéans un délai de 3 jours, un email est envoyé au client afin de lui demander s’il souhaite modifier certaines informations de son TAF (ex. le budget, le titre ou la description). Si le client accepte de modifier son TAF, celui-ci sera soumis à une autre validation de Tafsquare et ensuite envoyé aux professionnels séléctionnés dans le cas d'une validation positive de Tafsquare.</p>
		<p>Si aucun professionnel ne s’est montré intéressé endéans un délai de 5 jours, un email de rappel est automatiquement envoyé aux professionnels séléctionnés.</p>
		<p>Si aucun professionnel ne s’est montré intéressé endéans un délai de 10 jours, un second email est envoyé au client afin de lui demander s’il souhaite modifier certaines informations de son TAF (ex. le budget, le titre ou la description). Si le client accepte de modifier son TAF, celui-ci sera soumis à une autre validation de Tafsquare et ensuite envoyé aux professionnels séléctionnés dans le cas d'une validation positive de Tafsquare.</p>
		<p>Au bout de 14 jours, le client reçoit un email pour lui signaler que le TAF est clôturé. Il sera cependant toujours visible pour le client jusqu’à ce que celui-ci le supprime.</p>
		<p>Lorsqu’un ou plusieurs professionnels se montrent intéressés par le TAF créé par le client, un email de notification est envoyé à ce dernier, l’invitant à se rendre sur sa page personnelle de TAF afin de contacter les professionnels intéressés.</p>
		<p>Pour pouvoir contacter les professionnels intéressés, le client devra commander à Tafsquare leurs données de contact, et payer le prix indiqué hors TVA endéans les 7 jours de la commande, afin de les obtenir.</p>
		<p>Dans le cas où le client a trois TAF en cours ayant donné lieu à des réponses positives de professionnels intéressés, il ne pourra créer de TAF supplémentaire tant qu’il n’aura pas passé com- mande et honoré le paiement des données de contact des professionnels intéressés, et ce pour les trois TAFs en cours.</p>
		<p>Le client est seul responsable pour prendre contact avec les professionnels intéressés et conclure le TAF. Tafsquare décline également toute responsabilité dans le déroulement du TAF.</p>
		<p>Dans les 14 jours suivant la date de fin du TAF, le client recevra un mail de Tafsquare afin d’évaluer le professionnel sélectionné.</p>
		<p>Le client s’engage à évaluer le professionnel de manière objective et à faire preuve de modération dans les termes utilisés pour ses évaluations. Il s'interdira tout propos contraire aux règles de bonne conduite.</p>
		<p>Le client accepte par principe le système d'évaluation. Dans le cas où une évaluation contreviendrait aux règles de bonne conduite, le professionnel en informerait Tafsquare conformément aux dispositions des présentes conditions générales relatives aux réclamations.</p>
		<p>Si un professionnel estime qu'une évaluation ne vient pas du client pour lequel il a réalisé un TAF, il en informera Tafsquare. Tafsquare suspendra alors l'évaluation, et enverra un courrier à l'évaluateur afin d’obtenir des explications sur l’évaluation. Dans le cas où l’évaluateur refuse de fournir à Tafsquare toute explication lui permettant de confirmer sa relation avec le profession- nel, l'évaluation ne sera pas rétablie sur le site.</p>
		<p>En cas d’évaluation négative vis-à-vis du professionnel, celui-ci accepte de réagir uniquement de manière modérée. En cas d’évaluation contraire aux règles de bonne conduite vis-à-vis du professionnel, celui-ci accepte de ne réagir qu’en conformité avec les dispositions relatives aux réclamations.
		S’il s’avère que l’évaluation est effectivement contraire aux règles de bonne conduite, celle-ci pourra être supprimée par Tafsquare.</p>
		<p>Réclamations</p>
		<p>Les éventuelles réclamations devront être formulées par écrit, dans un délai de 7 jours après la survenance du fait donnant lieu à réclamation. A défaut, elles ne pourront être prises en compte. Si une réclamation s’avère fondée, Tafsquare aura le choix entre le remboursement du client ou la réparation en nature.</p>
		<p>Garanties</p>
		<p>Le client s’engage à ne jamais utiliser le site ou la platerforme du site mis à disposition par Tafsquare à des fins promotionnelles.</p>
		<p>Le professionnel s’engage à exécuter la prestation de service en bon père de famille, ainsi qu’à prendre toutes les dispositions nécessaires à la bonne réalisation du TAF. Plus spécifiquement, le professionnel s’engage à ne sélectionner que les Tafs pour la réalisation desquels il dispose de la compétence et de l’expérience nécessaires.</p>
		<p>Tafsquare ne garantit par conséquent en aucun cas la bonne réalisation du TAF, et ne peut être tenu pour responsable en cas de manquement du professionnel dans la réalisation du TAF.</p>
		<p>Le client bénéficie d’une garantie de conformité des prestations fournies par rapport aux prestations initialement demandées. En cas d’anomalie détectée durant cette période, Tafsquare en assurera gratuitement et dans les meilleurs délais la correction, sous réserve que les anomalies détectées aient été dûment signalées à Tafsquare.</p>
		<p>Sont expressément exclues de la garantie de conformité les prestations demandées à la suite d’une intervention ou d’une modification non autorisées, d’une erreur de manipulation ou d’une utilisation non conforme par le client, ou encore à la suite d’une anomalie engendrée par une intervention du client ou d'un tiers.</p>
		<p>
		Tafsquare déclare que les résultats des prestations qui seraient protégés par le droit de la propriété intellectuelle constituent des créations originales. Dans le cas où il aurait fait appel à des intervenants extérieurs pour réaliser tout ou partie des prestations, il déclare avoir obtenu tous les droits et autorisations nécessaires pour exécuter ces services.</p>
		<p>Par conséquent, Tafsquare garantit le client contre toute action, réclamation, allégation, revendication ou opposition de la part de toute personne invoquant un droit de propriété intellectuelle ou industrielle, ou un acte de concurrence déloyale, sur tout ou partie des éléments livrés, utilisés ou plus généralement mis en œuvre pour les besoins de l’exécution des prestations.</p>
		<p>Responsabilité de Tafsquare</p>
		<p>Le client reconnaît et accepte que toutes les obligations dont est débiteur Tafsquare sont exclusivement de moyens et que Tafsquare n’est responsable que de son dol et de sa faute lourde. Dans l’hypothèse où le client démontre l’existence d’une faute lourde ou dolosive dans le chef de Tafsquare, le préjudice dont le client peut postuler la réparation comprend uniquement le dommage matériel résultant directement de la faute imputée à Tafsquare à l’exclusion de tout autre dommage et ne pourra, en toute hypothèse, dépasser 75% (hors taxes) du montant effec- tivement payé par le client en exécution de la commande ou de la création d’un TAF.</p>
		<p>
		Le client reconnaît également que Tafsquare n’est pas responsable des éventuels dommages directs ou indirects causés par les services fournis, par la création d’un TAF, par la réalisation ou par la non réalisation d’un TAF, tels que notamment le manque à gagner, l’augmentation des frais généraux, la perte de clientèle, etc.</p>
		<p>Tafsquare ne procédant à aucune vérification d’ordre technique au niveau du TAF créé, les informations fournies par le client concernant ce TAF, et notamment les éventuels manquements d’informations ou erreurs d’informations, n’engagent en aucun cas la responsabilité de Tafsquare.</p>
		<p>Tafsquare n’est de même pas responsable en cas de communication de données erronées par le client ou par un professionnel, ou en cas de commande effectuée, de TAF créé, en son nom par une tierce personne.</p>
		<p>Propriété intellectuelle</p>
		<p>Les informations, logos, dessins, marques, modèles, slogans, chartes graphiques, etc, accessibles au travers du site internet, du catalogue, ou de la base de données de Tafsquare sont proté- gés par le droit de la propriété intellectuelle.</p>
		<p>Sauf convention contraire expresse et préalable, le client n’est pas autorisé à modifier, reproduire, louer, emprunter, vendre, distribuer ou créer des œuvres dérivées basées en tout ou partie sur les éléments présents sur le site internet ou le catalogue de Tafsquare.</p>
		<p>Internet & nouvelles technologies</p>
		<p>Le client connaît les restrictions et les risques liés à l'utilisation d'internet ou de tout autre moyen par lequel le site est actuellement ou sera à l'avenir mis à disposition. Le client connaît aussi les risques de stockage et de transmission d'informations par voie numérique ou électronique. Le client accepte que Tafsquare ne peut être tenu pour responsable de tout dommage causé par l'utilisation du site (ainsi que des éventuelles applications) ou d'internet, suite aux risques précités. Le client accepte en outre que les communications électroniques échangées et les backups réalisés par Tafsquare puissent servir de preuve.</p>
		<p>Traitement des données à caractère personnel</p>
		<p>Dans le cadre du processus de commande, le client qui commande doit communiquer des données à caractère personnel le concernant telles que son nom, prénom, adresse email, numéro TVA ou numéro d’entreprise, compte bancaire, références, numéro de téléphone portable, numéro de téléphone, et adresse. Ces données sont récoltées pour l’utilisation et la gestion du site, la gestion des demandes, commandes, et calendriers de TAF, la gestion des comptes utilisateurs, ainsi que, de manière générale, pour le traitement des commandes. Elles ne seront trans- mises aux partenaires de Tafsquare que dans le cas où le client marque expressément son accord.</p>
		<p>Tafsquare traite les données recueillies de manière confidentielle et conformément aux dispositions nationales et internationales, dont entre autres la loi Belge du 8 décembre 1992, relative à la protection de la vie privée à l’égard des traitements de données à caractère personnel, modifiée par la loi du 11 décembre 1998.</p>
		<p>
		Le client pourra, à tout moment, moyennant demande écrite datée et signée envoyée à Tafsquare, et après avoir justifié de son identité (en joignant une copie de la carte d'identité) obtenir gratuitement la communication écrite des données à caractère personnel le concernant qui ont été recueillies par Tafsquare, ainsi que, le cas échéant, la rectification de celles qui seraient inexactes, incomplètes ou non pertinentes, ou leur suppression.</p>
		<p>La copie de ses données lui sera communiquée au plus tard 45 jours après la réception de sa demande.</p>
		<p>Force majeure, cas fortuit et imprévision</p>
		<p>
		Tafsquare ne peut être tenu pour responsable, tant sur le plan contractuel qu’extracontractuel, en cas d’inexécution, temporaire ou définitive, de ses obligations lorsque cette inexécution résulte d’un cas de force majeure ou fortuit. Seront notamment considérés comme des cas de force majeure ou fortuits, les événements suivants : 1) la perte ou la destruction totale ou par- tielle du système informatique de Tafsquare ou de sa base de données lorsque l’un ou l’autre de ces événements ne peut raisonnablement pas être directement imputé à Tafsquare et qu’il n’est pas démontré que Tafsquare a omis de prendre les mesures raisonnables permettant de prévenir l’un ou l’autre de ces événements, 2) les tremblements de terre, 3) les incendies, 4) les inondations, 5) les épidémies, 6) les actes de guerre ou de terrorisme, 7) les grèves, déclarées ou non, 8) les lock-out, 9) les blocus, 10) les insurrections et émeutes, 11) un arrêt de fourniture d’énergie (telle que l’électricité), 12) une défaillance du réseau Internet ou du système de stockage des données, 13) une défaillance du réseau de télécommunications, 14) une perte de connec- tivité au réseau Internet ou au réseau de télécommunications dont dépend Tafsquare, 15) un fait ou une décision d’un tiers lorsque cette décision affecte la bonne exécution du présent contrat ou 16) toute autre cause échappant au contrôle raisonnable de Tafsquare.</p>
		<p>Si, en raison de circonstances indépendantes de la volonté de Tafsquare, l’exécution de ses obligations ne peut être poursuivie ou est simplement rendue plus onéreuse ou difficile, Tafsquare et le client s’engagent à négocier de bonne foi et loyalement une adaptation des conditions contractuelles dans un délai raisonnable en vue d’en restaurer l’équilibre. A défaut d’accord dans un délai raisonnable, chacune des parties pourra invoquer la résiliation de la relation contractuelle les unissant sans dédommagement ou indemnité de quelque nature que ce soit.</p>
		<p>Dispositions diverses</p>
		<p>Illégalité</p>
		<p>L’éventuelle illégalité ou nullité d’un article, d’un paragraphe ou d’une disposition (ou partie d’un article, d’un paragraphe ou d’une disposition) ne saurait affecter de quelque manière la légalité des autres articles, paragraphes ou dispositions de ces conditions générales, ni non plus le reste de cet article, de ce paragraphe ou de cette disposition, à moins d’intention contraire évidente dans le texte.</p>
		<p>Titres</p>
		<p>Les titres utilisés dans les présentes conditions générales ne le sont qu’à des fins de référence et de commodité seulement. Ils n’affectent en rien la signification ou la portée des dispositions qu’ils désignent.</p>
		<p>Totalité et intégralité de l’entente</p>
		<p>
		Les présentes conditions générales ainsi que les conditions d’utilisation du site web : http://www.tafsquare.com représentent la totalité et l’intégralité de l’entente intervenue entre les parties. Aucune déclaration, représentation, promesse ou condition non contenue dans les présentes conditions générales ne peut et ne doit être admise pour contredire, modifier ou affecter de quelque façon que ce soit les termes de celles-ci.</p>
		<p>En outre, les présentes conditions générales ainsi que les conditions d’utilisation du site web : http://www.tafsquare.com se substituent à tout accord éventuellement intervenu antérieurement entre les parties et s’appliquent à tout nouvel accord.</p>
		<p>Loi applicable et juridictions compétentes</p>
		<p>Toute contestation relative aux services fournis par Tafsquare, de même que toute contestation quant à la validité, l’interprétation ou l’exécution des présentes conditions générales sera sou- mise au droit belge et de la compétence exclusive des tribunaux de l’arrondissement judiciaire du Hainaut.</p>
		<p></p>

			<div class="col-sm-12 col-centered">
				<a href="#" class="btn-pages">Retour</a>
			</div>

	</div><!--row-->
</div><!--container-->



 <?php include 'includes/footer.php' ?>
</body>
