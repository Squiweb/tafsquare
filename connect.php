<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>


  <header class="small_header light_header">

    <div class="bg">
      <img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
    </div>

    <div class="container vertical_align">
      <div class="row row-centered">
        <div class="col-md-12 col-centered">
          <h1 class="titre-mauve">Connectez-vous</h1>
        </div>
      </div>
    </div>

  </header>


<section class="connectez-vous">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-2">

        <div class="form-input col-md-8 field-group inscription-form">
          <label for="email">E-mail</label>
          <input type="text" name="email" value="" placeholder="Entrez votre e-mail" id="email">


          <label for="mdp">Mot de passe</label>
          <input type="text" name="mdp" value="" placeholder="Entrez votre mot de passe" id="mdp">
          <input type="checkbox" name="cgu" value="true" id="cgu">
          <label for="cgu"  class="checkbox-label"><span></span>Se rappeller de moi</label>
          <button type="button" class="btn-pages" data-dismiss="modal">Connexion</button>
          <a class="forgetpsw" href="#">Mot de passe oublié ?</a>
        </div>

        <div class="col-md-6">
          <div class="separator"></div>
          <h3>Pas encore membre ?</h3>
          <a class="simplelink" href="#">Inscrivez-vous sur tafsquare</a>
        </div>

      </div>


    </div>
  </div>
</section>

<?php include 'includes/footer.php' ?>
