<?php
##
## Cette page est une page standard pour Tafsquare

##
## Parce que sur chaque page, il faudra accéder la DB, on charge db_connexion.php
## qui contient établi une connexion à la DB.
##
include_once './includes/db_connexion.php';

##
## Certaines fonctions sont communes à toutes les pages (login, session_start, ...)
## On charge functions.php qui contient ces fonctions.
##
include_once './includes/functions.php';

##
## On n'utilise pas la commande sessions_start
## On utilise une version 'améliorée' et mieux sécurisée.
##
sec_session_start();

##
## Quelques variables utiles pour la navigation
## Attention: dans $actual_link, il faut mettre https dès que les pages sont chez Ikoula
##
$actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['actual_link'] = $actual_link;
$page_title="create-taf";

if (isset($_GET['met'])) {
	$metierselect = $_GET['met'];
} else {
	$metierselect = "";
}

if (isset($_GET["l"])) {
	// Si on arrive sur cette page venant d'un mail, on connait la langue à utliser
	$_SESSION['actual_lang'] = strtoupper($_GET["l"]);
}

include_once './includes/fonctions_chargement.php';
charge_langue($page_title);
charge_layout();
charge_navigation();

?>

<script src="libs/js/awesomplete.js" async></script>

<form role="form" action="includes/process-create-taf.php" method="post" class="inscription-form inscription-container create-taf" onsubmit="return(pressevalider());"> 

<header class="small_header light_header create-taf-header">

	<div class="bg">
		<img src="images/photo-8.jpg">
	</div>



	<div class="container verticalalign">
			<div class="row row-centered">
				<div class="col-sm-12 col-centered find-job">
					<h1 class="text-center"><?php echo $lang['CREETUNTAF']; ?></h1>
					<div class="container-cent">
						<div class="col-sm-6">
							<div id="form-choisi-metier">
									<?php
									// Si il y a 'met', alors on va cherche le nom du metier pour le mettre dans le champs
									if ($metierselect <> "") {
										switch (strtoupper($_SESSION['actual_lang'])) {
											case 'FR':
												$reqSQL = "SELECT c.Id, categorienom FROM categories c JOIN secteurs s ON categoriesecteurid = s.id JOIN domaines d ON s.secteursdomaine = d.id 
												           WHERE categoriesignifiante=1 AND c.Id = ? LIMIT 1;";
												break;
											case 'NL':
												$reqSQL = "SELECT c.Id, c.`categorienom-nl` FROM categories c JOIN secteurs s ON categoriesecteurid = s.id JOIN domaines d ON s.secteursdomaine = d.id 
												           WHERE categoriesignifiante=1 AND c.Id = ? LIMIT 1;";
												break;
											case 'EN':
												$reqSQL = "SELECT c.Id, c.`categorienom-en` FROM categories c JOIN secteurs s ON categoriesecteurid = s.id JOIN domaines d ON s.secteursdomaine = d.id 
												           WHERE categoriesignifiante=1 AND c.Id = ? LIMIT 1;";
												break;
											default:
												$reqSQL = "SELECT c.Id, categorienom FROM categories c JOIN secteurs s ON categoriesecteurid = s.id JOIN domaines d ON s.secteursdomaine = d.id 
												           WHERE categoriesignifiante=1 AND c.Id = ? LIMIT 1;";
												break;
										}

										if ($stmt = $mysqli->prepare($reqSQL)) {
											$stmt->bind_param("i",$metierselect);
										  $stmt->execute();    // Exécute la déclaration.
										  $stmt->store_result();
										  $stmt->bind_result($idmetier, $nommetier);
										  $stmt->fetch();
										  echo '<input autofocus class="find_job" id="choisimetier" list="listedesmetiers" data-minchars=1 data-autofirst=true;" placeholder="' . $lang['CHOISSISEZUNMETIER'] . '" value="' . $nommetier . '" />';
										}
									} else {
										echo '<input autofocus class="find_job" id="choisimetier" list="listedesmetiers" data-minchars=1 data-autofirst=true;" placeholder="' . $lang['CHOISSISEZUNMETIER'] . '" value="" />';
									}

									?>

									<datalist id="listedesmetiers"><?php
										switch (strtoupper($_SESSION['actual_lang'])) {
											case 'FR':
												$reqSQL = "SELECT c.Id, categorienom FROM categories c JOIN secteurs s ON categoriesecteurid = s.id JOIN domaines d ON s.secteursdomaine = d.id 
										               WHERE categoriesignifiante=1 ORDER by categorienom;";
												break;
											case 'NL':
												$reqSQL = "SELECT c.Id, c.`categorienom-nl` FROM categories c JOIN secteurs s ON categoriesecteurid = s.id JOIN domaines d ON s.secteursdomaine = d.id 
										               WHERE categoriesignifiante=1 ORDER by c.`categorienom-nl`;";
												break;
											case 'EN':
												$reqSQL = "SELECT c.Id, c.`categorienom-en` FROM categories c JOIN secteurs s ON categoriesecteurid = s.id JOIN domaines d ON s.secteursdomaine = d.id 
										               WHERE categoriesignifiante=1 ORDER by c.`categorienom-en`;";
												break;
											default:
												$reqSQL = "SELECT c.Id, categorienom FROM categories c JOIN secteurs s ON categoriesecteurid = s.id JOIN domaines d ON s.secteursdomaine = d.id 
										               WHERE categoriesignifiante=1 ORDER by categorienom;";
												break;
										}

										if ($stmt = $mysqli->prepare($reqSQL)) {
										  $stmt->execute();    // Exécute la déclaration.
										  $stmt->store_result();
										  $stmt->bind_result($idmetier, $nommetier);

											while ($stmt->fetch()) {
												echo '<option data-value="' . $idmetier . '">' . $nommetier . '</option>';

										  # fin du While
										  }


										}
										?> 
									</datalist>
									<input type="hidden" name="me" id="answerInput-hidden">
							</div>
						</div>
						<div class="col-sm-6">
							<section class="not-found add-metier" id="metierpastrouve">
								<h4><?php echo $lang['VOUSNETROUVEZPAS']; ?></h4>
								<p><?php echo $lang['REMPLISSEZNOTREFORMULAIRE']; ?></p>
							</section>

						</div>
					</div>
				</div>
			</div>
		</div>
</header>

<nav class="nav-inscription">
	<a href="#" class="active col-sm-4 col-xs-12" id="etape1" onclick="affiche_ecran(1)"><?php echo $lang['ETAPE1_TITRE']; ?></a>
	<a href="#" class="col-sm-4 col-xs-12" id="etape2" onclick="affiche_ecran(2)"><?php echo $lang['ETAPE2_TITRE']; ?></a>
	<a href="#" class="col-sm-4 col-xs-12" id="etape3" onclick="affiche_ecran(3)"><?php echo $lang['ETAPE3_TITRE']; ?></a>
</nav>

<div id="googlemaps"></div>						

<div class="container">
	<div class="row">
		<div class="col-sm-6 form-box">

				<fieldset id="ecretape1" class="createtaf">

					<div class="form-bottom">
						<div id="form-date-debut">
							<div class="form-input col-sm-6 field-group">
								<label for="date-debut"><?php echo $lang['LA_DATEDEBUT']; ?><span class="asterix">*</span></label>
								<input type="text" class="date date-picker" name="dd" value="" placeholder="jj/mm/aaaa" id="date-debut">
								<div class="tva-icon fa fa-calendar-o form-icon"></div>								
							</div>
						</div>

						<div id="form-date-fin">
							<div class="form-input col-sm-6 field-group">
								<label for="date-debut"><?php echo $lang['LA_DATEFIN']; ?><span class="asterix">*</span></label>
								<input type="text" class="date date-picker" name="df" value="" placeholder="jj/mm/aaaa" id="date-fin">
								<div class="tva-icon fa fa-calendar-o form-icon"></div>								
							</div>
						</div>

						<div id="form-adresse-taf">
							<div class="form-input col-sm-12 field-group">
								<label for="adresse-taf"><?php echo $lang['LA_ADRESSE']; ?><span class="asterix">*</span></label>
								<input type="text" class="date" name="ad" placeholder="<?php echo $lang['PH_ADRESSE']; ?>" id="autocomplete" value=""></input>
								<div class="tva-icon fa fa-map-marker form-icon"></div>
							</div>
						</div>

						<div id="form-titre">
							<div class="form-input col-sm-6 field-group">
								<label for="titre-taf"><?php echo $lang['LA_TITRE']; ?><span class="asterix">*</span></label>
								<input type="text" class="date" name="ti" value="" placeholder="<?php echo $lang['PH_TITRE']; ?>" id="titre-taf">
								<div class="tva-icon fa fa-pencil form-icon"></div>
							</div>
						</div>

						<div id="form-budget">
							<div class="form-input col-sm-6 field-group">
								<label for="budget"><?php echo $lang['LA_BUDGET']; ?><span class="asterix">*</span></label>
								<input type="text" class="date" name="bu" value="" placeholder="<?php echo $lang['PH_BUDGET']; ?>" id="budget">
								<div class="tva-icon fa fa-credit-card form-icon"></div>
							</div>
						</div>

						<div id="form-descri">
							<div class="form-input col-sm-12">
								<label for="exp-description" class="motivation"><?php echo $lang['LA_DESCRIPTION']; ?><span class="asterix">*</span></label>
								<textarea name="de" id="exp-description" rows="8" cols="40" placeholder="<?php echo $lang['PH_DESCRIPTION']; ?>"></textarea>
								<span class="note">*<?php echo $lang['MENTIONOBLIGATOIRE']; ?></span>
							</div>
						</div>

						<button type="button" class="btn-pages btn-next" id="suivant1"><?php echo $lang['PB_SUIVANT']; ?></button>

					</div>


				</fieldset>

	      <fieldset id="ecretape2" class="createtaf">

					<div class="col-md-4 col-md-offset-1 perimetre_container">
						<h3><?php echo $lang['SELECTIONNEZDESPRESTA']; ?></h3>
						<div class="years perimetre-range cf">
							<p><?php echo $lang['SOIT1']; ?></p>
							<div id="groupeselectionkm">
								<div class="y" id="kmdistance">
									<p>
										10 km
									</p>
									<span class="triangle-slider-perim"></span>
								</div>

								<input class="perimetre_slide" type="range" name="sd" id="sliderdistance" ="10" value="10" max="300" min="0">
									<div class="lessmore">
										<div class="icon-less fa fa-minus" id="pluspetit" onclick="diminueRayon()" onmouseover="" style="cursor: pointer;"></div>
										<div class="icon-more fa fa-plus" id="plusgrand" onclick="augmenteRayon()" onmouseover="" style="cursor: pointer;"></div>
									</div>
								</div>
						</div>

						<div class="regions">
							<p><?php echo $lang['SOIT2']; ?></p>								
							<ul>
								<?php
										if ($stmt = $mysqli->prepare("SELECT Id, provincenom  
										                              FROM provinces ORDER by provincenom;")) {
										  $stmt->execute();    // Exécute la déclaration.
										  $stmt->store_result();
										  $stmt->bind_result($idprovince, $nomprovince);

											while ($stmt->fetch()) {
												echo '<li>';
													echo '<input type="checkbox" name="province" value="' . $idprovince  . '" id="' . $nomprovince . '" onclick="clickSurProvince()" >';
													echo '<label for="' . $nomprovince . '" class="checkbox-label"><span></span>' . $nomprovince . '</label>';
												echo '</li>';
						
										  # fin du While
										  }
										}
								?>
							</ul>
							<br>
						</div>
						<input type="hidden" name="pr" id="province-hidden">

					</div> <!-- fin du div perimetre avec province -->

					<div class="perimetre_form">
					<div class="col-md-6 col-md-offset-1 number ">
						<p class="number_independants" id="numprestafaires">
							<?php echo $lang['ACTUELLEMENT']; ?><span class="num" id="nbprestafaires">?</span><?php echo $lang['INDEPENDANT']; ?>
						</p>
					</div>
					</div>

					<div class="nav-btns col-sm-12 nav-map">
						<button type="button" class="btn-pages btn-previous" id="precedent2"><?php echo $lang['PB_PRECEDENT']; ?></button>
						&nbsp;
						<button type="button" class="btn-pages btn-next" id="suivant2"><?php echo $lang['PB_SUIVANT']; ?></button>	
					</div>

	      </fieldset>
	      
	      <fieldset id="ecretape3" class="createtaf">
	      	<div class="container create_validation">
						<div class="row">
							<h2 class="col-sm-12"><?php echo $lang['RIENNOUBLIER']; ?></h2>

							<div class="nav-btns col-sm-12 nav-map">
								<button type="button" class="btn-pages btn-previous" id="precedent3"><?php echo $lang['PB_PRECEDENT']; ?></button>
								&nbsp;
								<button type="submit" class="btn-pages btn-next" id="valider3"><?php echo $lang['PB_VALIDER']; ?></button>
							</div>
							
							<div class="col-sm-4">
								<i class="fa fa-search"></i>
								<h3><?php echo $lang['LA_METIER']; ?></h3>
								 <div id="ecrvalidemetier"><p>???</p></div>
							</div>

							<div class="col-sm-4">
								<i class="fa fa-calendar-o "></i>
								<h3><?php echo $lang['LA_DATEDEBUT']; ?></h3>
								<div id="ecrvalidedtdeb"><p>???</p></div>
							</div>

							<div class="col-sm-4">
								<i class="fa fa-calendar-o "></i>
								<h3><?php echo $lang['LA_DATEFIN']; ?></h3>
								<div id="ecrvalidedtfin"><p>???</p></div>
							</div>

							<div class="col-sm-4">
								<i class="fa fa-map-marker"></i>
								<h3><?php echo $lang['LA_ADRESSE']; ?></h3>
								<div id="ecrvalideadresses"><p>???</p></div>
							</div>

							<div class="col-sm-4">
								<i class="fa fa-credit-card"></i>
								<h3><?php echo $lang['LA_BUDGET']; ?></h3>
							 <div id="ecrvalidebudget"><p>???€</p></div>
							</div>

							<div class="col-sm-4">
								<i class="fa fa-bullseye"></i>
								<h3><?php echo $lang['LA_CIBLE']; ?></h3>
							 <div id="ecrvalidenbprestafaires"><p>???</p></div>
							</div>

							<div class="col-sm-4 float_none">
								<i class="fa fa-comment-o"></i>
								<h3><?php echo $lang['LA_TITRE']; ?></h3>
								<div id="ecrvalidetitre"><p>???</p></div>
							</div>

							<div class="col-sm-12 description">
								<i class="fa fa-newspaper-o"></i>
								<h3><?php echo $lang['LA_DESCRIPTION']; ?></h3>
								<div id="ecrvalidedescription"><p>???</p></div>
							</div>

						</div>
					</div>
	      </fieldset>
	  </div>
	</div>
</div>
</form>


<?php
##
## layout_pied-xx.inc.php contient le pied de page de la page HTML
##
charge_pied();

?>
<script>
jsvarlangue = '<?php echo strtolower($_SESSION['actual_lang']); ?>';

</script>

		<script src="libs/js/jquery.backstretch.min.js"></script>
		<script src="libs/js/retina-1.1.0.min.js"></script>

		<script src="https://maps.google.com/maps/api/js?sensor=false&language=<?php echo strtolower($_SESSION['actual_lang']); ?>"></script>
		<script src="https://maps.googleapis.com/maps/api/js?signed_in=false&libraries=places&callback=initAutocomplete&language=<?php echo strtolower($_SESSION['actual_lang']); ?>" async defer></script> 

<script>

var compteCp2d = new Array();
var compteProv2d = new Array();
var deville;
var placelat = 0;
var placelng = 0;


$(function () {
//var dateToday = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
var dateToday = new Date();	
      $('#date-debut').datetimepicker({
      	locale: '<?php echo strtolower($_SESSION['actual_lang']); ?>',
				minDate: dateToday ,
      	widgetPositioning: {
      		horizontal: 'auto',
      		vertical: 'bottom'
      	},
				format: 'DD/MM/YYYY',
        useCurrent: false //Important! See issue #1075					
      });

      $('#date-fin').datetimepicker({
      	locale: '<?php echo strtolower($_SESSION['actual_lang']); ?>',
				minDate: dateToday ,        	
      	widgetPositioning: {
      		horizontal: 'auto',
      		vertical: 'bottom'
      	},
				format: 'DD/MM/YYYY',
        useCurrent: false //Important! See issue #1075
      });

      $("#date-debut").on("dp.change", function (e) {
          $('#date-fin').data("DateTimePicker").minDate(e.date);
      });

      $("#date-fin").on("dp.change", function (e) {
          $('#date-debut').data("DateTimePicker").maxDate(e.date);
      });
  });

jQuery(document).ready(function() {

    /*
        Form
    */


    $('.inscription-form fieldset:first-child').fadeIn('slow');
    
    $('.inscription-form input[type="text"], .inscription-form input[type="password"], .inscription-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    // next step
    $('.inscription-form .btn-next').on('click', function() {

        var etape1 = document.getElementById('etape1');
        var etape1active = etape1.className.indexOf("active");
        var etape2 = document.getElementById('etape2');
        var etape2active = etape2.className.indexOf("active");
        var etape3 = document.getElementById('etape3');
        var etape3active = etape3.className.indexOf("active");

        if (etape1active >= 0) {
            var etapecurr = 1
        }
        if (etape2active >= 0) {
            var etapecurr = 2
        }
        if (etape3active >= 0) {
            var etapecurr = 3
        }

        var onBouge = false;

        switch(etapecurr) {
           case 1:
              if (valide_ecran1()) {
                  affiche_ecran(2);
              }
              break;
           case 2:
                affiche_ecran(3);
                break;
           case 3:
                // C'est en fait le bouton 'Valider'
                // Il n'y a rien à faire vu qu'il y a le form submit
                break;
        }

    });
    
    // previous step
    $('.inscription-form .btn-previous').on('click', function() {

        var etape1 = document.getElementById('etape1');
        var etape1active = etape1.className.indexOf("active");
        var etape2 = document.getElementById('etape2');
        var etape2active = etape2.className.indexOf("active");
        var etape3 = document.getElementById('etape3');
        var etape3active = etape3.className.indexOf("active");

        if (etape1active >= 0) {
            var etapecurr = 1
        }
        if (etape2active >= 0) {
            var etapecurr = 2
        }
        if (etape3active >= 0) {
            var etapecurr = 3
        }

        switch(etapecurr) {
           case 1:
                alert("Bizarre, il n'y a pas d'étape 0");
                break;
           case 2:
                affiche_ecran(1);
                break;
           case 3:
                affiche_ecran(2);
           break;
        }
        scroll(0,0);

    });
   
});

function affiche_ecran(etapenouv) {

        var etape1 = document.getElementById('etape1');
        var etape1active = etape1.className.indexOf("active");
        var etape2 = document.getElementById('etape2');
        var etape2active = etape2.className.indexOf("active");
        var etape3 = document.getElementById('etape3');
        var etape3active = etape3.className.indexOf("active");

        if (etape1active >= 0) {
            var etapecurr = 1
        }
        if (etape2active >= 0) {
            var etapecurr = 2
        }
        if (etape3active >= 0) {
            var etapecurr = 3
        }

        var onBouge = false;         

        switch(etapecurr) {
         case 1:
              if (valide_ecran1()) {
                document.getElementById("etape1").className = "col-sm-4 col-xs-12";
                $('#ecretape1').fadeOut(400);
                onBouge = true;
              }
              break;
         case 2:
              document.getElementById("etape2").className = "col-sm-4 col-xs-12";
              $('#ecretape2').fadeOut(400);
              onBouge = true;              
              break;
         case 3:
              document.getElementById("etape3").className = "col-sm-4 col-xs-12";
              $('#ecretape3').fadeOut(400);
              onBouge = true;              
              break;
        }

        if (onBouge) {
          switch(etapenouv) {
           case 1:
                document.getElementById("etape1").className = "active col-sm-4 col-xs-12";
                document.getElementById('metierpastrouve').style.visibility = 'visible';
                document.getElementById('googlemaps').style.visibility = 'hidden';
                document.getElementById('choisimetier').disabled = false;
                $('#ecretape1').fadeIn();
                break;
           case 2:
                document.getElementById("etape2").className = "active col-sm-4 col-xs-12";
                document.getElementById('metierpastrouve').style.visibility = 'hidden';
                document.getElementById('googlemaps').style.visibility = 'visible';
                document.getElementById('choisimetier').disabled = true;
                $('#ecretape2').fadeIn();              
                break;
           case 3:
                var tmpChamps = document.getElementById("choisimetier").value;
                var tmpValideChamps = document.getElementById("ecrvalidemetier");
                tmpValideChamps.innerHTML = "<p>" + tmpChamps + "</p>";

                var tmpChamps = document.getElementById("date-debut").value;
                var tmpDebut = moment(tmpChamps + " 00:00:00", "DD/MM/YYYY HH:mm:ss");                
                var tmpChamps = moment(tmpChamps,"DD/MM/YYYY");
                var tmpChamps = tmpChamps.locale('<?php echo strtolower($_SESSION['actual_lang']);?>').format('dddd DD MMM YYYY');
                var tmpValideChamps = document.getElementById("ecrvalidedtdeb");
                tmpValideChamps.innerHTML = "<p>" + tmpChamps + "</p>";

                var tmpChamps = document.getElementById("date-fin").value;
                var tmpFin = moment(tmpChamps + " 23:59:59", "DD/MM/YYYY HH:mm:ss");
                var tmpChamps = moment(tmpChamps,"DD/MM/YYYY");
                var tmpChamps = tmpChamps.locale('<?php echo strtolower($_SESSION['actual_lang']);?>').format('dddd DD MMM YYYY');
                var tmpValideChamps = document.getElementById("ecrvalidedtfin");
                var nbjours = Math.round(tmpFin.diff(tmpDebut, 'days', true));
                if (nbjours > 1) {
                  tmpValideChamps.innerHTML = "<p>" + tmpChamps + " (" + nbjours + " <?php echo $lang['JOURS']; ?>)</p>";
                } else {
                  tmpValideChamps.innerHTML = "<p>" + tmpChamps + " (" + nbjours + " <?php echo $lang['JOUR']; ?>)</p>";
                }

                var tmpChamps = document.getElementById("autocomplete").value;
                var tmpValideChamps = document.getElementById("ecrvalideadresses");
                tmpValideChamps.innerHTML = "<p>" + tmpChamps + "</p>";

                var tmpChamps = document.getElementById("budget").value;
                var tmpValideChamps = document.getElementById("ecrvalidebudget");
                tmpValideChamps.innerHTML = "<p>" + tmpChamps + "</p></div>";

                var tmpChamps = document.getElementById("titre-taf").value;
                var tmpValideChamps = document.getElementById("ecrvalidetitre");
                tmpValideChamps.innerHTML = "<p>" + tmpChamps + "</p>";

                var tmpChamps = document.getElementById("exp-description").value;
                tmpChamps = tmpChamps.replace(/[\n]/g, '<br>');
                var tmpValideChamps = document.getElementById("ecrvalidedescription");
                tmpValideChamps.innerHTML = "<p>" + tmpChamps + "</p>";


                document.getElementById("etape3").className = "active col-sm-4 col-xs-12";
                document.getElementById('metierpastrouve').style.visibility = 'hidden';
                document.getElementById('googlemaps').style.visibility = 'hidden';
                document.getElementById('choisimetier').disabled = true;
                $('#ecretape3').fadeIn();
                break;
        }
        scroll(0,0);      

        if (etapenouv > 1) {
            // On recalcul les Prestafaire au cas où on aurait changé de métier
            var leMetier = document.getElementById('answerInput-hidden').value;
    
            mySelectPrestafaires(mySelectPrestafairesReadData, leMetier, placelat, placelng);

        }
      }

}

function mySelectPrestafaires(callback, metierId, delat, delng) {
    xmlhttp=new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                callback(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET", "includes/select-prestaf-inc.php?metierid=" + metierId + "&delat=" + delat + "&delng=" + delng, true);
        xmlhttp.send(null);
}

function mySelectPrestafairesReadData(sData) {
    // Le système retourne un string composé de deux parties séparées par le |
    // La première partie est le nombre de PresTAFaires par km - sDataSplitted[0]
    // La seconde partie est le nomdre de PesTAFaire par province -  - sDataSplitted[1]

    //alert(sData);

    sDataSplitted = sData.split("|")
    compteCp2d = [];
    compteProv2d = [];    
    // On peut maintenant traiter les données sans encombrer l'objet XHR.
    if (sDataSplitted[0] == "FAIL") {
        alert("Y'a eu un problème- Codes Postaux");
    } else {
        // On va éclater le string de retour en un tableau à 2 dim
        var compteCp = new Array();
        compteCp = sDataSplitted[0].split(",");

        for (var i = 0; i < compteCp.length; i++) {
            compteCp2d[i] = compteCp[i].split(":");
        }
    }

    if (sDataSplitted[1] == "FAIL") {
        alert("Y'a eu un problème - Province");
    } else {
        var compteProv = new Array();
        var tmparray = new Array();        
        compteProv = sDataSplitted[1].split(",");
        for (var i = 0; i < compteProv.length; i++) {
          compteProv2d[i] = compteProv[i].split(":");
        }
    }

    combienDePrestafaires(compteCp2d,compteProv2d);
}

function combienDePrestafaires(compteCp2d, compteProv2d) {
    var rayon = parseInt(document.getElementById('sliderdistance').value);    
    var nbprestafaires = 0;
    var maxrayon = rayon;
    var lapluslongdist = parseInt(compteCp2d[compteCp2d.length-1][0]);

    maxrayon = lapluslongdist;

    // provinceselected contient la liste des provinces cochées
    var checkboxes = document.getElementsByName('province');
    var provinceselected = "";
    // On remet à zéro le zone cachée des provinces
    document.getElementById('province-hidden').value = "";

    for (var i=0, n=checkboxes.length;i<n;i++) {
      if (checkboxes[i].checked) 
      {
        provinceselected += ","+checkboxes[i].value;
      }
    }
    if (provinceselected) provinceselected = provinceselected.substring(1);

    if (provinceselected) {
      // On fait disparaître le slider pour le rayon
      document.getElementById('groupeselectionkm').style.visibility = 'hidden';
      // On place dans le champs caché la liste des provinces sélectionnées (pour l'envoi du form)
      document.getElementById('province-hidden').value = provinceselected;
      // Il y a au moins une province de sélectionnée, alors on additionne les cases cochées
      var provinceselectedarray = JSON.parse("[" + provinceselected + "]");
      for (var i=0, n=provinceselectedarray.length;i<n;i++) {
        provinceid = provinceselectedarray[i];
        for (var j = 0; j < compteProv2d.length; j++) {
          if (compteProv2d[j][1] == provinceid) {
            nbprestafaires += parseInt(compteProv2d[j][2]);
            j = compteProv2d.length // on force à sortie de la boucle
          }
        }
      }

    } else {
      // On fait apparaître le slider pour le rayon
      document.getElementById('groupeselectionkm').style.visibility = 'visible';
      // Pas de province sélécionnées, alors on compte avec le rayon
      var i = 0;
      while (parseInt(compteCp2d[i][0]) <= rayon) {
          nbprestafaires += parseInt(compteCp2d[i][1]);
          i++;
          if (i == compteCp2d.length) {
              break;
          }
      }
    }

    var numprestafaires = document.getElementById("numprestafaires");
    var numecrvalideprestafaires = document.getElementById("ecrvalidenbprestafaires");    

    if (nbprestafaires > 1) {
        numprestafaires.innerHTML = "<?php echo $lang['ACTUELLEMENT']; ?> <span class='num' id='nbprestafaires'>" + nbprestafaires + "</span> <?php echo $lang['INDEPENDANTS']; ?>.";
        numecrvalideprestafaires.innerHTML = nbprestafaires + " <?php echo $lang['INDEPENDANTS']; ?>.";
    } else {
        numprestafaires.innerHTML = "<?php echo $lang['ACTUELLEMENT']; ?> <span class='num' id='nbprestafaires'>" + nbprestafaires + "</span> <?php echo $lang['INDEPENDANT']; ?>.";
        numecrvalideprestafaires.innerHTML = nbprestafaires + " <?php echo $lang['INDEPENDANT']; ?>.";
    }

    // On désactive le bouton 'Valider' si aucun prestAfaires n'est concerné.
    if (nbprestafaires == 0) {
      document.getElementById('valider3').style.visibility = 'hidden';
    } else {
      document.getElementById('valider3').style.visibility = 'visible';      
    }

}

var p = document.getElementById("sliderdistance"),
    res = document.getElementById("kmdistance");

p.addEventListener("input", function() {
    res.innerHTML = "<p>" + p.value + " km</p><span class='triangle-slider-perim'></span>";
    combienDePrestafaires(compteCp2d);
}, false); 


// On clique sur le bouton +
function augmenteRayon() {
    var p = document.getElementById("sliderdistance"),
        res = document.getElementById("kmdistance");
        p.value = parseInt(p.value) + 1;
        res.innerHTML = "<p>" + p.value + " km</p><span class='triangle-slider-perim'></span>";
        combienDePrestafaires(compteCp2d);        
}

// On clique sur le bouton -
function diminueRayon() {
    var p = document.getElementById("sliderdistance"),
        res = document.getElementById("kmdistance");
        p.value = parseInt(p.value) - 1;
        res.innerHTML = "<p>" + p.value + " km</p><span class='triangle-slider-perim'></span>";
        combienDePrestafaires(compteCp2d);        
}

// On clique sur une checkbox d'une province
function clickSurProvince() {
  combienDePrestafaires(compteCp2d,compteProv2d);
}

$(document).ready(function(){
    document.getElementById('googlemaps').style.visibility = 'hidden';
});

var var_map;


function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
      {types: ['geocode'], componentRestrictions: {country: 'be'}
    });

   google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            // Get address_components
          for (var i = 0; i < place.address_components.length; i++)
              {
                var addr = place.address_components[i];
                var getCountry;
                if (addr.types[0] == 'locality')
                  deville = addr.long_name;
              }


            placelat = place.geometry.location.lat();
            placelng = place.geometry.location.lng();

//            alert(place.name);
//            alert(place.geometry.location.lat());
//            alert(place.geometry.location.lng());
            var zoominitial = 12;
            var zoomfacteur = 12;

                      var var_location = new google.maps.LatLng(placelat,placelng);

                        rayon = document.getElementById("sliderdistance");

                      var var_mapoptions = {
                        center: var_location,
                        scrollwheel: false,
                        streetViewControl: false,
                        styles: [ { featureType:'water', stylers:[{color:'#F2F2F2'},{visibility:'on'}] },
                                  { featureType:'landscape', stylers:[{color:'#FFFFFF'}] },
                                  { featureType:'road', stylers:[{saturation:-100},{lightness:45}] },
                                  { featureType:'road.highway', stylers:[{visibility:'simplified'}] },
                                  { featureType:'road.arterial', elementType:'labels.icon', stylers:[{visibility:'off'}] },
                                  { featureType:'administrative', elementType:'labels.text.fill', stylers:[{color:'#ADADAD'}] },
                                  { featureType:'transit', stylers:[{visibility:'off'}] },
                                  { featureType:'poi', stylers:[{visibility:'off'}]  }]
                      };

                      var var_marker = new google.maps.Marker({
                        position: var_location,
                        map: var_map,
                        title:"Emplacement de votre TAF"});

                      var var_map = new google.maps.Map(document.getElementById("googlemaps"), var_mapoptions);
                      var_marker.setMap(var_map); 

                        // Add circle overlay and bind to marker
                        var circle = new google.maps.Circle({
                          map: var_map,
                          radius: parseInt(rayon.value) * 1000,    // 10 miles in metres
                          fillColor: '#AA0000'
                        });
                        circle.bindTo('center', var_marker, 'position');
                        var_map.fitBounds(circle.getBounds());                        

                        google.maps.event.addDomListener(
                           document.getElementById('sliderdistance'), 'change', function() {
                               rayon = parseInt(document.getElementById('sliderdistance').value);
                               circle.setRadius(rayon * 1000);
                               var_map.fitBounds(circle.getBounds());                        
                           });

                        google.maps.event.addDomListener(
                           document.getElementById('plusgrand'), 'click', function() {
                               rayon = parseInt(document.getElementById('sliderdistance').value);
                               circle.setRadius(rayon * 1000);
                               var_map.fitBounds(circle.getBounds());
                           });

                        google.maps.event.addDomListener(
                           document.getElementById('pluspetit'), 'click', function() {
                               rayon = parseInt(document.getElementById('sliderdistance').value);
                               circle.setRadius(rayon * 1000);
                               var_map.fitBounds(circle.getBounds());
                           });

    });

 
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}

$(".find_job").on("focusout", function() {
    if (verifmetier()) {
        document.getElementById("form-choisi-metier").className = "";      
    } else {
        document.getElementById("form-choisi-metier").className += " has-error has-feedback";
    }
});

function verifmetier() {
    var metierok = false;
    var options = document.querySelectorAll('#listedesmetiers option');
    var hiddenInput = document.getElementById('answerInput-hidden');
    var inputValue = $('#choisimetier').val();

    hiddenInput.value = 0;

    for(var i = 0; i < options.length; i++) {
        var option = options[i];
        if(option.innerText.toUpperCase() === inputValue.toUpperCase()) {
            hiddenInput.value = option.getAttribute('data-value');
            document.getElementById('choisimetier').value = option.innerText;
            document.getElementById('answerInput-hidden').value = option.getAttribute('data-value');
            metierok = true
            break;
        }
    }
    return metierok;
}

function valide_ecran1() {
    // Mettre DEV pour le développement en local.  Cela évite de devoir remplir tout le formulaire
    //var enviro = "DEV";
    var enviro = "QA";
    var rep = true;

    var choisimetier=document.getElementById('choisimetier').value;
    if (choisimetier == null || choisimetier == "") {
        document.getElementById("form-choisi-metier").className += " has-error has-feedback";
        scroll(0,0);        
        rep = false;
    } else {
        document.getElementById("form-choisi-metier").className = "";
    }

    // Check si le metier existe 
    if (verifmetier()) {
        document.getElementById("form-choisi-metier").className = "";      
    } else {
        document.getElementById("form-choisi-metier").className += " has-error has-feedback";
        scroll(0,0);        
        rep = false;        
    }

    // Pour les tests, si DEV alors je ne force pas tous les champs
    // Seuls le metier, l'adresse sont obligatoires

    if (enviro !="DEV") {
      var datedebut=document.getElementById('date-debut').value;

      if (datedebut == null || datedebut == "") {
          document.getElementById("form-date-debut").className += " has-error has-feedback";
          rep = false;
      } else {
          document.getElementById("form-date-debut").className = "";
      }

      var datefin=document.getElementById('date-fin').value;    
      if (datefin == null || datefin == "") {
          document.getElementById("form-date-fin").className += " has-error has-feedback";
          rep = false;
      } else {
          document.getElementById("form-date-fin").className = "";
      }

      var tmpdatedeb = datedebut.split("/");
      var tmpdatefin = datefin.split("/");

      tmpdatedeb = new Date(tmpdatedeb[2], tmpdatedeb[1] - 1, tmpdatedeb[0]);
      tmpdatefin = new Date(tmpdatefin[2], tmpdatefin[1] - 1, tmpdatefin[0]);

      if (rep) {
        if (tmpdatedeb > tmpdatefin) {
            document.getElementById("form-date-debut").className += " has-error has-feedback";
            document.getElementById("form-date-fin").className += " has-error has-feedback";          
            rep = false;
        } else {
            document.getElementById("form-date-debut").className = "";
            document.getElementById("form-date-fin").className = "";
        }
      }

      
      var budget=document.getElementById('budget').value;
      if (budget == null || budget == "") {
          document.getElementById("form-budget").className += " has-error has-feedback";
          rep = false;
      } else {
          document.getElementById("form-budget").className = "";
      }

      var titre=document.getElementById('titre-taf').value;
      if (titre == null || titre == "") {
          document.getElementById("form-titre").className += " has-error has-feedback";
          rep = false;
      } else {
          document.getElementById("form-titre").className = "";
      }

      var descri=document.getElementById('exp-description').value;
      if (descri == null || descri == "") {
          document.getElementById("form-descri").className += " has-error has-feedback";
          rep = false;
      } else {
          document.getElementById("form-descri").className = "";
      }

    }

    var adretaf=document.getElementById('autocomplete').value;    
    if (adretaf == null || adretaf == "") {
        document.getElementById("form-adresse-taf").className += " has-error has-feedback";
        rep = false;
    } else {
        document.getElementById("form-adresse-taf").className = "";
    }

    if (placelat == 0 || placelng == 0) {
        document.getElementById("form-adresse-taf").className += " has-error has-feedback";
        rep = false;
    } else {
        document.getElementById("form-adresse-taf").className = "";
    }

    if (rep == false) {
      return false;
    } else {
      return true;
    }
}

function pressevalider() {
  // On submit le form uniquement si on est sur l'écran 3
  var etape3 = document.getElementById('etape3');
  var etape3active = etape3.className.indexOf("active");

  if (etape3active >= 0) {
    return (true);
  } else {
    return (false);
  }
}



</script>

  </body>
</html>

