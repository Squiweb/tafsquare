<!-- Header -->
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>


<header class="small_header light_header">
	<!-- SMALL HEADER -->
	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-sm-12 col-centered find-job">
				<h1 class="text-center">Créer un taf</h1>
			</div>
		</div>
	</div>
</div>

</header>

<!-- CREATE NAV -->
<nav class="nav-inscription cf">
	<a href="create_taf.php" class="validated col-sm-4 col-xs-12">1. Description TAF</a>
	<a href="create_perimetre.php" class="active col-sm-4 col-xs-12">2. Périmètre de recherche</a>
	<a href="create_validation.php" class="col-sm-4 col-xs-12">3. Validation</a>
</nav>

<!-- MAP -->
<div class="map-overlay"></div>
<div id="map"></div>
<!-- FORM CONTAINER -->
<div class="container">
	<div class="row">

<form class="inscription-form  inscription-container perimetre_form container" action="#" method="post">

		<div class="col-md-4 col-md-offset-1 perimetre_container">
			<h3>Sélectionnez des prestataires</h3>
			<div class="years perimetre-range cf">

				<p>
					1. Soit à l'aide d'un périmètre de recherche (en km)
				</p>

					<div class="y">
						<p>
							10 km
						</p>
						<span class="triangle-slider-perim"></span>
					</div>


				<input class="perimetre_slide" type="range" name="years" value="10" max="30" min="0">
				<div class="lessmore">
					<a href="#" class="icon-less fa fa-minus"></a>
					<a href="#" class="icon-more fa fa-plus"></a>
				</div>

			</div>
			<div class="regions">
				<p>
					2. Soit en sélectionnant une ou plusieurs région(s) :
				</p>
				<ul>
					<li>
						<input type="checkbox" name="limburg" value="true" id="limburg">
						<label for="limburg"  class="checkbox-label"><span></span>Limburg</label>
					</li>
					<li>
						<input type="checkbox" name="oost" value="true" id="oost">
						<label for="oost"  class="checkbox-label"><span></span>Oost-Vlanderen</label>
					</li>
					<li>
						<input type="checkbox" name="vlams" value="true" id="vlams">
						<label for="vlams"  class="checkbox-label"><span></span>Vlams Brabant</label>
					</li>
					<li>
						<input type="checkbox" name="west" value="true" id="west">
						<label for="west"  class="checkbox-label"><span></span>West-Vlanderen</label>
					</li>
					<li>
						<input type="checkbox" name="hainaut" value="true" id="hainaut">
						<label for="hainaut"  class="checkbox-label"><span></span>Hainaut</label>
					</li>
					<li>
						<input type="checkbox" name="liege" value="true" id="liege">
						<label for="liege"  class="checkbox-label"><span></span>Liège</label>
					</li>
					<li>
						<input type="checkbox" name="wallon" value="true" id="wallon">
						<label for="wallon"  class="checkbox-label"><span></span>Brabant-Wallon</label>
					</li>
					<li>
						<input type="checkbox" name="luxembourg" value="true" id="luxembourg">
						<label for="luxembourg"  class="checkbox-label"><span></span>Luxembourg</label>
					</li>
					<li>
						<input type="checkbox" name="namur" value="true" id="namur">
						<label for="namur"  class="checkbox-label"><span></span>Namur</label>
					</li>
					<li>
						<input type="checkbox" name="bruxelles" value="true" id="bruxelles">
						<label for="bruxelles"  class="checkbox-label"><span></span>Bruxelles</label>
					</li>
				</ul>
			</div>
		</div><!-- /.col-sm-6 -->
		<div class="col-md-6 col-md-offset-1 number ">
			<p class="number_independants">
				Actuellement, votre TAF sera envoyé à <span class="num">5</span> indépendants
			</p>

		</div>

</form>
<div class="nav-btns col-sm-12 nav-map cf">
	<button type="button" name="button" class="btn-pages">Retour</button>
	<button type="button" name="button" class="btn-pages">Suivant</button>
</div>
</div>
</div>
<script>
function initMap() {
	var mapDiv = document.getElementById('map');
	var map = new google.maps.Map(mapDiv, {
		center: {lat: 44.540, lng: -78.546},
		zoom: 8
	});
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?callback=initMap"
async defer></script>
<?php include 'includes/footer.php' ?>
