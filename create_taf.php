<!-- Header -->
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>


<header class="small_header light_header create-taf-header">
	<!-- SMALL HEADER -->
	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-sm-12 col-centered find-job">
				<h1 class="text-center">Créer un taf</h1>
				<div class="container-cent">
					<div class="col-sm-6 col-go"><input type="text" name="name" value="" placeholder="Choisissez un métier" class="find_job">
					</div>
					<div class="col-sm-6">
						<section class="not-found add-metier">
							<h4>Vous ne trouvez pas votre métier?</h4>
							<p>
								Remplissez notre <a href="inscription-new-taf.php">formulaire de demande d’ajout de métier</a>
								aﬁn que l’on puisse l’ajouter sur notre site
							</p>
						</section></div>

					</div>
				</div>
			</div>
		</div>
	</div>

</header>

<!-- CREATE NAV -->
<nav class="nav-inscription cf">
	<a href="create_taf.php" class="active col-sm-4 col-xs-12">1. Description TAF</a>
	<a href="create_perimetre.php" class="col-sm-4 col-xs-12">2. Périmètre de recherche</a>
	<a href="create_validation.php" class="col-sm-4 col-xs-12">3. Validation</a>
</nav>
<!-- FORM CONTAINER -->
<div class="container">
	<div class="row">
		<form class="inscription-form  inscription-container create-taf"  action="#" method="post">
			<div class="form-input col-sm-6 field-group">
				<label for="date-debut">Date de début<span class="asterix">*</span></label>
				<input type="date" class="date" max="2030-12-31" min="2015-01-01" name="date-debut" value="" placeholder="" id="date-debut">
				<div class="tva-icon fa fa-calendar-o form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="form-input col-sm-6 field-group">
				<label for="date-fin">Date de fin<span class="asterix">*</span></label>
				<input type="date" class="date" max="2030-12-31" min="2015-01-01" name="date-fin" value="" placeholder="" id="date-fin">
				<div class="tva-icon fa fa-calendar-o form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="col-sm-6 field-group adresse-taf">
				<div class="form-input">
					<label for="adresse-taf">Adresse du TAF<span class="asterix">*</span></label>
					<input type="text" name="adresse-taf" value="" placeholder="Adresse du TAF" id="adresse-taf">
					<div class="tva-icon fa fa-map-marker form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
				</div>
			</div>


			<div class="form-input col-sm-6 field-group">
				<label for="titre-taf">Titre du TAF<span class="asterix">*</span></label>
				<input type="text" name="titre-taf" value="" placeholder="Titre du TAF" id="titre-taf">
				<div class="tva-icon fa fa-pencil form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="form-input col-sm-6 field-group">
				<label for="budget">Budget<span class="asterix">*</span></label>
				<input type="text" name="budget" value="" placeholder="€" id="budget">
				<div class="tva-icon fa fa-credit-card form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>
			<div class="form-input col-sm-12">
				<label for="exp-description" class="motivation">Description du TAF<span class="asterix">*</span></label>
				<textarea name="exp-description" id="exp-description" rows="8" cols="40" placeholder="Indiquez-nous les raisons pour lesquelles vous souhaitez soumettre ce nouveau métier"></textarea>
				<span class="note">*mention obligatoire</span>
			</div>
		</form>
		<div class="nav-btns col-sm-12 cf">
			<button type="button" name="button" class="btn-pages">Retour</button>
			<button type="button" name="button" class="btn-pages">Continuer</button>
		</div>
	</div>
</div>
<?php include 'includes/footer.php' ?>
