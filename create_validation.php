<!-- Header -->
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>

<header class="small_header light_header">
	<!-- SMALL HEADER -->
	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-sm-12 col-centered find-job">
				<h1 class="text-center">Créer un taf</h1>
			</div>
		</div>
	</div>
</div>

</header>

<!-- CREATE NAV -->
<nav class="nav-inscription cf">
	<a href="create_taf.php" class="validated col-sm-4 col-xs-12">1. Description TAF</a>
	<a href="create_perimetre.php" class="validated col-sm-4 col-xs-12">2. Périmètre de recherche</a>
	<a href="create_validation.php" class="active col-sm-4 col-xs-12">3. Validation</a>
</nav>
<!-- FORM CONTAINER -->

<div class="container create_validation">
	<div class="row">
		<h2 class="col-sm-12">N’avez-vous rien oublié ?<br />
			Validez votre TAF pour confirmer sa publication.</h2>
			<div class="col-sm-4">
				<i class="fa fa-search"></i>
				<h3>Métier</h3>
				<p>Informaticien</p>
			</div>
			<div class="col-sm-4">
				<i class="fa fa-calendar-o "></i>
				<h3>Date du début</h3>
				<p>21 janv. 2016</p>
			</div>
			<div class="col-sm-4">
				<i class="fa fa-calendar-o "></i>
				<h3>Date de fin</h3>
				<p>26 janv. 2016</p>
				</div>

				<div class="col-sm-4">
					<i class="fa fa-map-marker"></i>
					<h3>Adresse du TAF</h3>
					<p>Rue des frères Wright, 6041, Charleroi</p>
				</div>
				<div class="col-sm-4">
					<i class="fa fa-credit-card"></i>
					<h3>Budget</h3>
					<p>2000</p><span class="euro">€</span>
				</div>
				<div class="col-sm-4">
					<i class="fa fa-bullseye"></i>
					<h3>Cible</h3>
					<p>5 prestataires</p>
				</div>

				<div class="col-sm-4 float_none">
					<i class="fa fa-comment-o"></i>
					<h3>Titre du TAF</h3>
					<p>Informaticien Wordpress</p>
				</div>

				<div class="col-sm-12 description">
					<i class="fa fa-newspaper-o"></i>
					<h3>Description du taf</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
				<div class="nav-btns col-sm-12 cf">
					<button type="button" name="button" class="btn-pages">Retour</button>
					<button type="button" name="button" class="btn-pages">Valider</button>
				</div>
			</div><!-- /.ROW -->

		</div><!--container-->


		<?php include 'includes/footer.php' ?>
