<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>


<header class="small_header light_header title-mauve">

  <div class="bg">
    <img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
  </div>

  <div class="container vertical_align">
    <div class="row row-centered">
      <div class="col-md-12 col-centered">
        <h1>Désinscription</h1>
      </div>
    </div>
  </div>
</header>



<section class="message">
  <div class="container">
    <div class="row row_logo">
      <div class="background_logo col-sm-4">
        <img src="img/logo-bg.png" alt="Logo Tafsquare">
      </div>
      <div class="col-sm-8">
        <h2>Merci !</h2>
        <p>Nous avons bien enregistré le fait que vous ne<br>
        voulez plus faire partie de la base de donnée tafsquare.
      </p><br />
        <p class="bold patua">
          À bientôt...
        </p>
        <div class="separator"></div>
      </div>


    </div>
  </div>
</section>

<?php include 'includes/footer.php' ?>
