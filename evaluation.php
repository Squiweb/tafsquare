<?php
include 'includes/head.php';
include 'includes/navbar.php';
?>
<header class="small_header light_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1 class="asterix">Évaluation</h1>
			</div>
		</div>
	</div>

</header>

<section class="evaluation">
  <div class="container">
    <div class="row infos">
      <div class="col-sm-4">
        <p class="society">Société A</h3>
        <p class="prestataire">Robert Dutoit</p>
      </div>
      <div class="col-sm-4">
        <p class="city"><i class="fa fa-map-marker fa-3x"></i><span>Florennes</span></p>
      </div>
      <div class="col-sm-4">
        <p class="job">Charpentier</p>
      </div>
    </div>
    <div class="row stars">
      <div class="col-sm-12">
      <h4>Evaluez ce prestataire selon les critère suivants : </h4>
      <ul>
        <li>Qualité du travail :
          <div class="rating-stars rating-stars-hover">
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
          </div>
        </li>
        <li class="pro">Professionnalisme :<span class="hint">( Fiable, Responsable, Organisé )</span>
					<div class="rating-stars rating-stars-hover">
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
          </div>
        </li>
        <li>Respect des délais
					<div class="rating-stars rating-stars-hover">
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
          </div>
        </li>
        <li>Respect du budget
					<div class="rating-stars rating-stars-hover">
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
          </div>
        </li>
      </ul>
      </div>
    </div>
    <div class="row conclusion">
      <div class="col-sm-12">
      <h3>Conclusion</h3>
      <form class="evaluation-form" action="evaluation-confirmation.php" method="post">
        <div class="form-input field-group">
        <h4>Recommanderiez-vous ce prestataire ?</h4>
        <div class="radio">
          <input type="radio" name="recommandation" value="oui" id="rec-yes">
          <label for="rec-yes"><span></span>oui</label>
          <input type="radio" name="recommandation" value="non" id="rec-no">
          <label for="rec-no"><span></span>non</label>
        </div>
        </div>
        <div class="form-input field-group">
        <h4>Retravailleriez-vous avec ce prestataire ?</h4>
          <div class="radio">


          <input type="radio" name="ret" value="oui" id="ret-yes">
          <label for="ret-yes"><span></span>oui</label>
          <input type="radio" name="ret" value="non" id="ret-no">
          <label for="ret-no"><span></span>non</label>

          </div>
        </div>
          <div class="field-group commentaires">
            <label for="commentaitres">Commentaires</label>
            <textarea name="commentaires" rows="8" cols="40" placeholder="Inscrivez vos commentaires"></textarea>
          </div>
      </form>
      </div>
			<div class="nav-btns col-sm-12 cf">
				<button type="button" name="button" class="btn-pages">Valider</button>
			</div>
    </div>
  </div>
</section>

<?php include 'includes/footer.php' ?>
