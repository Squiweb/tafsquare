<!-- Header -->
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>
<header class="small_header light_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1 class="asterix">Évaluation</h1>
			</div>
		</div>
	</div>

</header>
	<!-- CONFIRM CONTAINER -->
	<div class="container confirm-mail">
		<div class="row row_logo">
			<div class="background_logo col-sm-4">
        <img src="img/logo-bg.png" alt="Logo Tafsquare">
      </div>
			<section class="col-sm-8">
				<h2>Merci !</h2>
				<p>
					Nous avons bien enregistré votre évaluation ! <br>
					Nous vous remercions pour le temps que vous y avez consacré.
				</p>
				<div class="cf">
					<button type="button" name="button" class="btn-pages">Accueil</button>
				</div>
			</section>
		</div>
	</div>

 <?php include 'includes/footer.php' ?>
