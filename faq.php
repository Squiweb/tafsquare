
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>
<header class="small_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1 class="yellow">Foire aux questions</h1>
			</div>
		</div>
	</div>

</header>


<div class="container faq">
	<div class="row">
		<div class="col-sm-12">
			<h3>Page métier : texte invitant à créer un TAF</h3>
			<p>
				Voulez-vous trouver un prestataire compétent qui accepte de réaliser votre TAF selon vos critères ?
				Choisissez un métier, précisez vos besoins et observez la magie de la collaboration.
			</p>


			<h3>Page « ne plus recevoir les mails »</h3>
			<p>
				Nous vous remercions de votre confiance. Vous pouvez continuer à consulter les TAFs en cours.
				Marquez votre intérêt sur les TAFs qui vous concernent. Avec un profil complet, donnez-vous surtout
				les meilleures chances d’être contacté par des commanditaires.
			</p>

			<h3>Page de remerciement d’avoir modifié un TAF</h3>
			<p>
				Vous avez modifié votre TAF avec succès. Il sera renvoyé à tous les indépendants correspondants à
				vos critères. Vous serez prévenu par email dès la première réponse à votre annonce.
			</p>

			<h3>Page de désintérêt par rapport à un TAF (je clique « ça n’intéresse pas »)</h3>
			<p>
				Nous avons bien noté votre désintérêt pour ce TAF. Marquez votre intérêt pour d’autres TAFs qui
				vous concernent. En plus, avec un profil complet, donnez-vous surtout les meilleures chances d’être
				contacté par des commanditaires.
			</p>




			<h3>1. Qui utilise TAFSQUARE ?</h3>

			Cette plateforme est réservée exclusivement aux indépendants et aux entreprises, c’est à dire

			qu’il vous faut impérativement un numéro de TVA ou un numéro d’entreprise pour pouvoir vous

			<h3>2. Pourquoi utiliser TAFSQUARE plutôt que d’appeler mes contacts?</h3>

			Utiliser TAFSQUARE, c’est se simplifier la vie. Vous ne perdrez plus votre temps à chercher et à

			appeler tous vos contacts pour leur expliquer 20 fois ce que vous attendez d’eux.

			<h3>3. Qui paie pour quoi ?</h3>

			Formule : paiement par TAF

			L’indépendant qui crée un TAF paie 65 € pour débloquer la page contenant les coordonnées des

			professionnels qui ont répondu favorablement. Il n’effectuera qu’un seul paiement par TAF, et ce, à

			la condition qu'un professionnel au moins ait montré de l'intérêt au TAF qu'il a soumis. Ce n'est

			qu'après avoir payé qu'il pourra visualiser les informations détaillées du ou des professionnels

			ayant montré de l'intérêt pour son TAF.

			Les indépendants à la recherche d’un TAF ne payent pas.

			dès qu’il reçoit au minimum une réponse d’un prestataire

			Formule : abonnement mensuel ou annuel

			- Abonnement mensuel : 12,50 €/mois, ce qui vous coûtera 150 €/an

			- Abonnement annuel : 120 €/an, soit une économie de 20%

			Dans les deux cas, Vous bénéficiez d’un accès illimité à la plate-forme pour traiter les TAF. Et vous

			communiquez aussi à volonté avez toutes les métiers.

			<h3>4. Quand utiliser TAFSQUARE ?</h3>

			Utiliser TAFSQUARE :

			 Dès que vous voulez créer un TAF. En quoi est-ce génial ?

			- Lorsque vous avez besoin d’un sous-traitant car il vous manque les ressources, les

			compétences, le temps, ou encore la flexibilité. Ou pour réduire le coût de l’intérim,

			voire même le coût du travail..

			- Si vous n’avez pas le temps de chercher dans votre réseau

			- Et puis notre réseau présente ses limites à un moment ou un autre

			 Ou dès que vous êtes à la recherche d’un TAF, comme prestataire, pour bosser..

			<h3>5. Comment finaliser la collaboration avec mon prestataire ?</h3>

			Dans le cas d’un paiement au TAF, acquittez-vous du montant de 65 € afin d’obtenir les

			coordonnées de votre prestataire et contactez-le pour finaliser la collaboration.  Votre

			responsabilité est maintenant de prendre contact avec les professionnels intéressés pour

			conclure le TAF.

			<h3>6. Puis-je indiquer mon numéro de téléphone dans le titre ou la description de mon taf ?</h3>

			Non, vous devez d’abord finaliser votre collaboration avant de contacter votre prestataire. De

			plus, Tafsquare se réserve le droit de suspendre ou de refuser la création d’un TAF si un numéro

			de téléphone est écrit dans le titre ou la description du TAF.

			<h3>7. Que faire si je ne trouve pas mon métier</h3>

			Si vous ne trouvez pas votre métier, vous pouvez le créer vous-même en cliquant sur le lien

			«Créer un métier ».  Ensuite, il vous suffit de choisir le secteur et d’inscrire votre métier. Vous

			pouvez également décrire votre métier. Cependant, avant que votre métier soit créé, Tafsquare

			doit le valider. Ce n’est qu’après cette validation que vous recevrez une notification vous

			confirmant la création du métier. S’il est validé, vous n’aurez plus qu’à retourner dans votre

			profil afin de rajouter votre métier.

			<h3>8. En quoi la description de mon profil est-elle importante ?</h3>

			Le client TAFSQUARE doit communiquer des données à caractère personnel le concernant telles

			que son nom, prénom, adresse email, numéro de TVA ou numéro d’entreprise, compte

			bancaire, références, numéro de téléphone portable, numéro de téléphone, et adresse. Ces

			données sont importantes pour l’utilisation et la gestion du site, la gestion des demandes,

			commandes, et calendriers de TAF, la gestion des comptes utilisateurs, ainsi que, de manière

			générale, pour le traitement des commandes. Elles ne seront transmises aux partenaires de

			Tafsquare que dans le cas où le client marque expressément son accord.

			<h3>9. Un prestataire est-il contacté à coup sûr ?</h3>

			<h3>10. Comment créer un TAF ?</h3>

			Pour soumettre un TAF, le client documente l’ensemble des informations qui lui sont demandées,

			s’inscrit sur le site ou s'authentifie et ensuite l'envoie aux professionnels sélectionnés sur base de

			leur localisation géographique.

			Le client recevra ensuite un email de Tafsquare validant le TAF, reprenant un récapitulatif du TAF

			qu’il a créé, et notamment le métier, les dates de début et de fin, l’adresse, le titre, la description,

			ainsi que le budget du TAF.

			Lorsque Tafsquare ne valide pas le TAF, un email expliquant les raisons de ce refus est envoyé au

			<h3>11. Tous les TAF peuvent-ils être crées ?</h3>

			Non, Tafsquare se réserve le droit de suspendre ou de refuser la création d’un TAF, notamment dans

			le cas où les données communiquées par le client s’avèrent manifestement erronées ou incomplètes,

			ou lorsque le TAF créé par le client n’est, à la seule discrétion de Tafsquare, pas licite, conforme aux

			règles habituelles de bonnes vies et mœurs, ou de manière générale conforme à une destination

			normale et aux finalités du site.

			Plus spécifiquement, Tafsquare se réserve le droit de suspendre ou de refuser la création d’un TAF

			lorsqu’un numéro de téléphone est écrit dans le titre ou la description du TAF, lorsque le montant

			proposé pour le TAF n’est pas réaliste eu égard aux usages habituels, lorsque les dates proposées

			pour le TAF ne sont pas cohérentes, ou encore lorsque le TAF n’a pas de rapport avec le métier

			sélectionné pour la création du TAF.

			<h3>12. Que faire si personne ne répond à mon TAF ?</h3>

			Si aucun professionnel ne s’est montré intéressé après 3 jours, un email est envoyé au client afin de

			lui demander s’il souhaite modifier certaines informations de son TAF (ex. le budget, le titre ou la

			description). Si le client accepte de modifier son TAF, celui-ci sera soumis à une autre validation de

			Tafsquare et ensuite envoyé aux professionnels séléctionnés dans le cas d'une validation positive de

			Si aucun professionnel ne s’est montré intéressé endéans un délai de 5 jours, un email de rappel est

			automatiquement envoyé aux professionnels séléctionnés.

			Si aucun professionnel ne s’est montré intéressé endéans un délai de 10 jours, un second email est

			envoyé au client afin de lui demander s’il souhaite modifier certaines informations de son TAF (ex. le

			budget, le titre ou la description). Si le client accepte de modifier son TAF, celui-ci sera soumis à une

			autre validation de Tafsquare et ensuite envoyé aux professionnels séléctionnés dans le cas d'une

			validation positive de Tafsquare.

			<h3>13. Quelle est la durée de vie d’un TAF ?</h3>

			La durée de vie d’un TAF est de 14jours. Le TAF ne sera plus visible après ce délai.

			<h3>14. Comment voir les TAFs qui pourraient m’intéresser ?</h3>

			Vous pouvez visualiser les TAFs susceptibles de vous intéresser en vous inscrivant sur TAFSQUARE et

			en recevant ainsi les TAFs par email. Ou en allant régulièrement sur Tafsquare.com afin de visualiser

			les TAFS proposés.

			<h3>15. Puis-je montrer un intérêt pour tout type de tafs ?</h3>

			Non, vous ne pouvez montrer votre intérêt que pour les Tafs correspondants à vos métiers (Max 3).
		</div>
	</div>
</div>



<?php include 'includes/footer.php' ?>
</body>
