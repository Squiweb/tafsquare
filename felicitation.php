<?php
include 'includes/head.php';
include 'includes/navbar.php';
include 'includes/small_header.php'
?>


<section class="message">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-2">
        <h2>Félicitations !</h2>
        <p>Votre intérêt a bien été pris en compte.<br>Votre profil sera communiqué à l’organisateur du TAF.</p>
				<p class="message">VOUS SEREZ PEUT-ÊTRE CONTACTÉ PROCHAINEMENT PAR LE COMMANDITAIRE. <span>(DÉLAI MOYEN 3 À 7 JOURS)</span></p>
      </div>

      <div class="col-md-12 text-right">
        <a href="#" class="btn">Retour</a>
      </div>

    </div>
  </div>
</section>

<?php include 'includes/footer.php' ?>
