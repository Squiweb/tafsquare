<!-- COOKIES -->
<div class="cookie-bar">
  <div class="col-md-10">
    <p>Notre site utilise des cookies pour fournir une expérience sur mesure à vos visiteurs. <a href="#">En savoir plus</a></p>
  </div>
  <div class="col-md-2 btn-col">
    <a href="#" class="btn-classic btn-cookies">D'accord</a>
  </div>
</div>
