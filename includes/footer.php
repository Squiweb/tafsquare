<!-- Footer -->

<footer>
  <div class="container">
    <div class="row row-centered">

      <div class="col-md-3 col-md-offset-2 height100">
        <form class="lang_dropdown_form verticalalign" action="#" method="post">
          <select class="lang_dropdown_select" name="">
            <option value="français">Français</option>
            <option value="anglais">Anglais</option>
          </select>
        </form>
      </div>

      <div class="col-sm-12 col-md-4 col-md-offset-2 contactus">
        <h3>Nous contacter</h3>
        <p>Email: <a href="infos@tafsquare.com">infos@tafsquare.com</a></p>
        <p>Communauté de professionnels</p>
        <h3 class="secondone">Parcourir le site</h3>
        <ul>
          <li><a href="#">Inscription</a></li>
          <li><a href="#">Choississez un métier</a></li>
          <li><a href="#">Faq</a></li>
        </ul>
      </div>

      <div class="col-md-8 col-centered joinus">
        <h4>Rejoignez-nous sur :</h4>
        <ul>
          <li class="twitter"><a href="#" class="fa fa-twitter icon-footer"></a></li>
          <li class="linkedin"><a href="#" class="fa fa-linkedin icon-footer"></a></li>
          <li class="facebook"><a href="#" class="fa fa-facebook icon-footer"></a></li>
          <li class="google-plus"><a href="#" class="fa fa-google-plus icon-footer"></a></li>
        </ul>
      </div>
    </div>

    <div class="footer-second">
      <div class="container">
        <div class="row">
          <p class="col-md-8 col-sm-12">2016 © TafSquare. All Rights Reserved. <a href="#">Conditions d'utilisations</a></p>
          <a href="#" class="footer-logo col-sm-2 col-md-offset-2"><img src="img/logo.png" alt="TafSquare"/></a>
        </div>
      </div>
    </div>
  </div>

</footer>

</div>


<!--<script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="js/main.js" type="text/javascript"></script> 						<!-- Main Javascript File -->
<script src="js/jquery.waypoints.min.js" type="text/javascript"></script>		<!-- Waypoints -->
<script src="js/inview.min.js" type="text/javascript"></script>  <!-- Inview Effect Feed -->
<script src="js/stickUp.js" type="text/javascript"></script> 		  <!-- StickUp -->
<script src="js/owl.carousel.js" type="text/javascript"></script> 		  <!-- OWl Carousel -->
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenLite.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="js/pointer_events_polyfill.js" type="text/javascript"></script> 						<!-- pointer-event IE9 -->
<script src="js/autoresize.js" type="text/javascript"></script> 						<!-- autoresize textarea -->



<!-- Change UA-XXXXX-X to be your site's ID -->
<!--<script>
window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
Modernizr.load({
  load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
});
</script>  -->

</body>
</html>
