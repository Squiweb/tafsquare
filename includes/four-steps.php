<section id="first_panel">
  <div class="container">

    <div class="row">

      <div class="col-md-12 col-centered">
        <h2>Simple <b>&</b> efficace.</h2>
        <p class="lead">Trouvez des pros pour faire face à tous les jobs, de toutes les tailles, par tous les temps</p>
      </div>

      <div class="col-md-12 col-centered">
        <ul class="four_up tiles">
          <li>
            <img src="img/icon/creertaf.png" alt="Créez un TAF">
            <h4>1. Créez un TAF</h4>
            <p>Dites-nous ce dont vous avez besoin, sélectionnez un métier, défnissez un budget, une date et un lieu. Votre annonce fera réagir les meilleurs spécialistes.</p>
          </li>
          <li>
            <img src="img/icon/consultreponse.png" alt="Consultez vos réponses">
            <h4>2. Consultez vos réponses</h4>
            <p>Après paiement, vous consultez la liste des indépendants intéressés par votre offre.</p>
          </li>
          <li>
            <img src="img/icon/collaboconfiance.png" alt="Collaborez en toute confiance">
            <h4>3. Collaborez en toute confiance</h4>
            <p>Choisissez votre prestataire sur base de son expérience, de ses références et de ses évaluations. Contactez-le pour fnaliser votre nouveau partenariat.</p>
          </li>
          <li>
            <img src="img/icon/travailfait.png" alt="Le travail est fait">
            <h4>4. Le travail est fait</h4>
            <p>Evaluez votre prestataire pour faciliter les recherches ultérieures.</p>
          </li>
        </ul>
      </div>

    </div>
  </div>
  <div>
  <div class="bandeau">
    <p>Rien n'est plus réjouissant qu'une nouvelle collaboration <a class="btn-bord">4 étapes</a></p>
    <a class="close-btn"><i class="fa fa-times"></i></a>
  </div>
</section>

<section class="bandeau_mobile totranslate">
  <p>Découvrez Tafsquare en <a class="btn-mobile-bandeau" href="four_steps_mobile.php">4 étapes</a></p>
  <a class="close-btn-mobile"><i class="fa fa-times"></i></a>
</section>
