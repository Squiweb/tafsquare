

  <header>
    <div class="bg">
      <img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
    </div>
    <div class="container verticalalign home_verticalalign">
      <div class="row row-centered">

        <div class="col-md-12 col-centered">
          <h1 class="title_home">Les meilleurs indépendants sont ici !</h1>
          <div class="compteur"><span>145 Métiers</span><span>70 000 indépendants</span></div>
        </div>

        <div class="col-md-6 choix">
          <h2>Trouvez un professionnel</h2>
          <p class="header-p">Vous êtes une entreprise ?<br>Donnez vos conditions et trouvez le prestataire rêvé.</p>
          <a class="btn-single header-btn" data-toggle="modal" data-target="#seConnecter">Je crée un Taf<sup>*</sup></a>
          <span class="prix header-prix">60€/TAF<br>PROMO : gratuit jusqu'au 01/04/2016</span>
        </div>
        <div class="col-md-6 choix">
          <h2>Recevez du TAF<sup>*</sup></h2>
          <p class="header-p">Vous êtes indépendant ou freelance ? <br>Inscrivez-vous et recevez des propositions de contrats.</p>
          <a class="btn-single header-btn">Je m'inscris</a>
          <span class="prix header-gratuit">Gratuit</span>
        </div>
      </div>
    </div>
    <div class="info"><sup>*</sup> Travail à faire</div>
    <!-- Trigger the modal with a button -->

  </header>

  <!-- Modal -->
  <div class="modal fade" id="seConnecter" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content seconnecter">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Se connecter</h3>
        </div>
        <div class="modal-body">
          <div class="form-input field-group">
            <label for="email">Email :</label>
            <input type="text" name="email" value="" placeholder="Entrez votre e-mail" id="email">
            <label for="mdp">Mot de passe :</label>
            <input type="text" name="mdp" value="" placeholder="Entrez votre mot de passe" id="mdp">
            <input type="checkbox" name="remember" value="true" id="remember">
            <label for="remember" class="checkbox-label"><span></span>Se rappeller de moi</label>
            <a class="forgetpw" data-toggle="modal" data-target="#forgetPw" href="#">Mot de passe oublié</a>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-pages" data-dismiss="modal">Se connecter</button>
        </div>
      </div>
    </div>
  </div>


  <!-- Modal Forget Password -->
  <div class="modal fade" id="forgetPw" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content forgetpw">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Mot de passe oublié : </h3>
        </div>
        <div class="modal-body">
          <div class="form-input field-group">
            <label for="email">Email ou nom d'utilisateur<span class="asterix">*</span></label>
            <input type="text" name="email" value="" placeholder="Entrez votre e-mail ou votre nom d'utilisateur" id="email">
            <div class="text-right">
              <button type="button" class="btn-pages" data-dismiss="modal">Envoyer</button>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <p>Vérifiez vos derniers mails reçus pour réinitialiser votre mot de passe</p>
        </div>
      </div>
    </div>
  </div>
