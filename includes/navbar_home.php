<div class="whole-container">

  <nav id="principal" class="totranslate">
    <a href="#" class="logo"><img src="img/logo.png" alt="TafSquare"/></a>


		<!-- IF NOT CONNECTED -->

    <ul class="principal_nav">
			<li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Langue <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">FR</a></li>
          <li><a href="#">NL</a></li>
          <li><a href="#">AN</a></li>
        </ul>
      </li>
      <li><a href="#">INSCRIPTION</a></li>
      <li><a href="#">SE CONNECTER</a></li>
    </ul>


		<!-- IF CONNECTED -->

		<!-- <ul class="profile-wrapper">
			<li>
				<div class="profile">
					<img src="https://secure.gravatar.com/avatar/2f05a5d3273d54e90f1ad6dca5eb97bc?s=47" />
					<a href="#" class="name">hello@supview.be</a>

					<ul class="menu">
						<li><a href="#">Profile</a></li>
						<li><a href="#">Mes Tafs</a></li>
						<li><a href="#">Se déconnecter</a></li>
					</ul>
				</div>
			</li>
		</ul> -->

  </nav>

  <div class="navToggle totranslate"><div class="icon"></div></div>
