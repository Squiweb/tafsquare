<?php
include 'includes/head.php';
include 'includes/four-steps.php';
include 'includes/navbar_home.php';
include 'includes/header.php';
include 'includes/cookies.php';
?>
<section id="latest_taf">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-centered">
        <h2>Nos derniers TAFs</h2>
        <p class="lead">Ils nous ont confié avoir besoin de vous</p>
      </div>

      <div class="col-sm-12 col-centered col_latest_taf">

        <ul class="swipe-taf">
          <!-- TAF -->
          <li class="taf middle-time">
            <span class="budget">Budget<br>1500€</span>
            <img src="http://www.placehold.it/400x260" alt="exemple">
            <span class="title">Charpentier</span>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
            </p>
            <div class="info-taf">
              <div class="col-md-12">
                <i class="fa fa-map-marker"></i> Charleroi
              </div>
              <div class="col-md-12">
                <i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
              </div>
            </div>
            <span class="expire">Expire dans 5 jours</span>
          </li>
          <!-- TAF -->
          <li class="taf end-time">
            <span class="budget">Budget<br>740€</span>
            <img src="http://www.placehold.it/400x260" alt="exemple">
            <span class="title">Peintre</span>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
            </p>
            <div class="info-taf">
              <div class="col-md-12">
                <i class="fa fa-map-marker"></i> Charleroi
              </div>
              <div class="col-md-12">
                <i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
              </div>
            </div>
            <span class="expire">Expire dans 2 jours</span>
          </li>
          <!-- TAF -->
          <li class="taf">
            <span class="budget">Budget<br>150€</span>
            <img src="http://www.placehold.it/400x260" alt="exemple">
            <span class="title">Squiweb</span>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
            </p>
            <div class="info-taf">
              <div class="col-md-12">
                <i class="fa fa-map-marker"></i> Bruxelles
              </div>
              <div class="col-md-12">
                <i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
              </div>
            </div>
            <span class="expire">Expire dans 14 jours</span>
          </li>
          <!-- TAF -->
          <li class="taf end-time">
            <span class="budget">Budget<br>1500€</span>
            <img src="http://www.placehold.it/400x260" alt="exemple">
            <span class="title">Magouilleur</span>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
            </p>
            <div class="info-taf">
              <div class="col-md-12">
                <i class="fa fa-map-marker"></i> Mons
              </div>
              <div class="col-md-12">
                <i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
              </div>
            </div>
            <span class="expire">Expire dans 2 jours</span>
          </li>
          <!-- TAF -->
          <li class="taf">
            <span class="budget">Budget<br>1500€</span>
            <img src="http://www.placehold.it/400x260" alt="exemple">
            <span class="title">Magouilleur</span>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
            </p>
            <div class="info-taf">
              <div class="col-md-12">
                <i class="fa fa-map-marker"></i> Lille
              </div>
              <div class="col-md-12">
                <i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
              </div>
            </div>
            <span class="expire">Expire dans 14 jours</span>
          </li>
        </ul>

      </div>

      <div class="col-md-12 col-centered">
        <a href="#" class="btn-single">Voir tous les TAFs</a>
      </div>
    </div>
  </div>
</section>

<section id="whyuse">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-centered">
        <h2>Pourquoi utiliser tafsquare ?</h2>
        <p class="lead">Les meilleurs sous-traitants du marché sont prêts à commencer aujourd'hui</p>
      </div>
      <div class="col-md-12 col-centered">

        <ul class="three_up tiles">
          <li>
            <img src="img/icon/ecocollabo.png" alt="L’économie collaborative">
            <h4>1. L’économie collaborative</h4>
            <p>Participez à l’économie de demain et créez de la valeur ensemble.</p>
          </li>

          <li>
            <img src="img/icon/winwin.png" alt="Le win-win sécurisé">
            <h4>2. Le win-win sécurisé</h4>
            <p>Les entreprises sélectionnent les profils les mieux cotés. Les indépendants ont des propositions gratuites.</p>
          </li>

          <li>
            <img src="img/icon/bonaffaire.png" alt="Bon pour vos affaires">
            <h4>3. Bon pour vos affaires</h4>
            <p>Beaucoup plus économique qu’un engagement en interne ou une collaboration hasardeuse.</p>
          </li>
        </ul>

        <ul class="three_up tiles">
          <li>
            <img src="img/icon/simplerapide.png" alt="Simple et rapide">
            <h4>4. Simple et rapide</h4>
            <p>Publiez vos annonces en moins de 2 minutes,trouvez un prestataire 4 fois plus vite qu’en agence.</p>
          </li>

          <li>
            <img src="img/icon/qualiverif.png" alt="Qualité vérifée">
            <h4>5. Qualité vérifée</h4>
            <p>Les profils de nos indépendants sont vérifiés et leur qualité est approuvée.</p>
          </li>

          <li>
            <img src="img/icon/votrechoix.png" alt="Votre choix">
            <h4>6. Votre choix</h4>
            <p>Choisissez librement votre prestataire. C’est vous qui pilotez la négociation.</p>
          </li>
        </ul>

      </div>
    </div>
  </div>
</section>

<section id="domains">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-centered">
        <h2>Quel est votre domaine ?</h2>
        <p class="lead">145 domaines d’activité, bien rangés en 6 catégories</p>
      </div>

      <div class="col-md-12 grille-domaines">

        <div class="col-md-9 domaine">
          <div class="bg"><img src="https://images.unsplash.com/photo-1447752875215-b2761acb3c5d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=e18a8e9c0b879cfb054cd6ee89ecf803" alt="exemple"></div>
          <a href="#" class="banner_link"></a>
          <h3 class="verticalalign">Immobilier</h3>
        </div>

        <div class="col-md-3 domaine">
          <div class="bg"><img src="https://images.unsplash.com/photo-1419312520378-cbd583837112?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=f2ab4f40f9d5a6cee0fcab65cba5c325" alt="exemple"></div>
          <a href="#" class="banner_link"></a>
          <h3 class="verticalalign">Santé</h3>
        </div>

        <div class="col-md-6 domaine">
          <div class="bg"><img src="https://images.unsplash.com/photo-1449265614232-03dfc33163a0?crop=entropy&fit=crop&fm=jpg&h=975&ixjsv=2.1.0&ixlib=rb-0.3.5&q=80&w=2125" alt="exemple"></div>
          <a href="#" class="banner_link"></a>
          <h3 class="verticalalign">Culture & Divertissement</h3>
        </div>

        <div class="col-md-6 domaine">
          <div class="bg"><img src="https://images.unsplash.com/photo-1414690165279-49ab0a9a7e66?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=d36ea3a39c51d6711a4ccbd1d37124b1" alt="exemple"></div>
          <a href="#" class="banner_link"></a>
          <h3 class="verticalalign">Web & Technologies</h3>
        </div>

        <div class="col-md-3 domaine">
          <div class="bg"><img src="https://images.unsplash.com/photo-1430651717504-ebb9e3e6795e?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=1850d66a67f2a46276a0bad9ae209581" alt="exemple"></div>
          <a href="#" class="banner_link"></a>
            <h3 class="verticalalign">Économie</h3>
        </div>

        <div class="col-md-9 domaine">
          <div class="bg"><img src="https://images.unsplash.com/36/yJl7OB3sSpOdEIpHhZhd_DSC_1929_1.jpg?crop=entropy&fit=crop&fm=jpg&h=975&ixjsv=2.1.0&ixlib=rb-0.3.5&q=80&w=2125" alt="exemple"></div>
          <a href="#" class="banner_link"></a>
          <h3 class="verticalalign">Commerce</h3>
        </div>

        <div class="col-md-12 domaine full">
          <div class="bg"><img src="https://images.unsplash.com/photo-1419312520378-cbd583837112?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=f2ab4f40f9d5a6cee0fcab65cba5c325" alt="exemple"></div>
          <a href="#" class="banner_link"></a>
          <h3 class="verticalalign">Santé</h3>
        </div>

      </div>

    </div>
  </div>
</section>

<section id="pricing">
  <div class="container">
    <div class="row">
      <h2>Nos tarifs</h2>
      <div class="col-sm-6">
        <?php include 'img/svg/pricing-s.svg'; ?>
        <h3>Standard</h3>
        <div class="pricing-cont">
        <p class="pricing">
          65€
        </p><span class="pertaf">/taf</span>
        <div class="jauge-left"></div>
        <p class="infos">
          Un besoin précis à un moment donné ? Payez à l’utilisation. Et trouvez le professionnel qui vous convient.
        </p>
        </div>
      </div>
      <div class="col-sm-6">
        <?php include 'img/svg/pricing-s.svg'; ?>
        <?php include 'img/svg/stars.svg'; ?>
        <h3>Illimité</h3>
        <div class="pricing-cont">
        <p class="pricing">
          12,5€
        </p><span class="pertaf">/mois</span>
        <div class="jauge-right"></div>
        <p class="infos">
          Vous bénéficiez d’un accès illimité à la plate-forme pour créer des TAFs.
        </p>
        </div>
      </div>
      <div class="col-sm-6 col-sm-offset-6">
        <a href="#" class="btn-single">Souscrire</a>
      </div>
    </div>
  </div>
</section>

<section id="testimonials">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-centered">
        <h2>Les mots sympas</h2>
      </div>
      <div class="col-md-12 makeitswipe">

        <div class="contenu-makeitswipe col-md-offset-1">
          <div class="col-md-5 testimonial">
            <blockquote>
              J’ai construit des relations mutuellement bénéfiques avec d’autres indépendants et je peux aujourd’hui répondre aux exigences de mon entreprise sans cesse croissante.
            </blockquote>
            <span class="name">Francis Stargasm</span>
            <span class="job">Entrepreneur</span>
          </div>
          <div class="col-md-5 testimonial">
            <blockquote>
              J’ai construit des relations mutuellement bénéfiques avec d’autres indépendants et je peux aujourd’hui répondre aux exigences de mon entreprise sans cesse croissante.
            </blockquote>
            <span class="name">Daniel Bat l'avoine</span>
            <span class="job">Entrepreneur</span>
          </div>
        </div>

        <div class="contenu-makeitswipe col-md-offset-1">
          <div class="col-md-5 testimonial">
            <blockquote>
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </blockquote>
            <span class="name">Francis Stargasm</span>
            <span class="job">Entrepreneur</span>
          </div>
          <div class="col-md-5 testimonial">
            <blockquote>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </blockquote>
            <span class="name">François Martin</span>
            <span class="job">Supview.be</span>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>

<section id="theyuse">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-centered">
        <h2>Ils utilisent tafsquare</h2>
      </div>
      <ul class="five_up tiles col-centered">
        <li><img src="http://www.placehold.it/300x150"></li>
        <li><img src="http://www.placehold.it/300x150"></li>
        <li><img src="http://www.placehold.it/300x150"></li>
        <li><img src="http://www.placehold.it/300x150"></li>
        <li><img src="http://www.placehold.it/300x150"></li>
        <li><img src="http://www.placehold.it/300x150"></li>
        <li><img src="http://www.placehold.it/300x150"></li>
        <li><img src="http://www.placehold.it/300x150"></li>
        <li><img src="http://www.placehold.it/300x150"></li>
        <li><img src="http://www.placehold.it/300x150"></li>
      </ul>
    </div>
  </div>
</section>


<?php include 'includes/footer.php' ?>
