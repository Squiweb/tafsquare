<!-- Header -->
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>


<header class="small_header light_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1>Une inscription
					<span>et bientôt de nouveaux contrats</span>
				</h1>
			</div>
		</div>
	</div>

</header>

<!-- CONFIRM CONTAINER -->

<!-- CONFIRM CONTAINER -->
<section class="message">
	<div class="container">
		<div class="row row_logo">
			<div class="background_logo col-sm-4">
				<img src="img/logo-bg.png" alt="Logo Tafsquare">
			</div>
			<div class="col-sm-8 confirmation">
				<h2>Félicitations !</h2>
				<p>
					Votre profil est à présent complet.<br>
					Vous pouvez le mettre à jour à tout moment via votre compte privé.
				</p>
				<p class="green confirmation_green">
					CHAQUE FOIS QUE VOUS RÉAGIREZ À UN TAF, <br>
					CES INFORMATIONS VOUS DÉMARQUERONT DE VOS CONCURRENTS.
				</p>
				<div class="cf">
					<button type="button" name="button" class="btn-pages">Créer un TAF</button>
					<button type="button" name="button" class="btn-pages">Voir les TAFs</button>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include 'includes/footer.php' ?>
