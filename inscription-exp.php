<!-- Header -->
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>
<header class="small_header light_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1>Une inscription
					<span>et bientôt de nouveaux contrats</span>
				</h1>
			</div>
		</div>
	</div>

</header>

<!-- INSCRIPTION NAV -->
<nav class="nav-inscription cf">
	<a href="inscription-form.php" class="validated col-sm-4 col-xs-12">1. Vos coordonnées</a>
	<a href="inscription-metiers.php" class="validated col-sm-4 col-xs-12">2. Vos métiers</a>
	<a href="inscription-exp.php" class="active col-sm-4 col-xs-12">3. Votre expérience</a>
</nav>


<!-- CONTAINER -->
<div class="container inscription-container exp-container">
	<section class="experiences">
		<div class="row">
		<p class="introduction col-sm-12">
			Cette page est votre meilleure alliée pour convaincre vos nouveaux partenaires.<br>
			<span class="bold">Montrez toutes vos facettes.</span> Pour chaque métier, encodez plusieurs spécialités et pour chaque spécialité, plusieurs références.
		</p>
		</div>
		<div class="metier-container">
			<div class="row">
				<div class="col-sm-6 name">
					<h4>Metier 1</h4>
					<p class="selected-job">
						Coach
					</p>
				</div>

				<div class="col-sm-6 years">

					<h4>Années d'expériences</h4>
					<ul>
						<li>Junior</li>
						<li>
							<p class="y">
								10 ans
							</p>
							<div class="triangle-slider"></div>
						</li>

						<li>Senior</li>
					</ul>
					<input type="range" class="slider-years range" name="years" value="10" max="30" min="0">

					<div class="lessmore">
						<a href="#" class="icon-less fa fa-minus"></a>
						<a href="#" class="icon-more fa fa-plus"></a>
					</div>

				</div>
			</div>

			<div class="row row-form">

				<!--<form class="experiences-formu" action="#" method="post">
					<div class="experiences-form cf">
						<div class="col-sm-3">
							<h4>Spécialité</h4>
							<input type="text" name="speciality-1" placeholder="Compétence ou service particulier">
							<input type="text" name="speciality-2" placeholder="Compétence ou service particulier">
							<input type="text" name="speciality-3" placeholder="Compétence ou service particulier">
						</div>
						<div class="col-sm-3">
							<h4>Référence 1</h4>
							<input type="text" name="ref-1-1" placeholder="Nom de l'entreprise">
							<input type="text" name="ref-2-1" placeholder="Nom de l'entreprise">
							<input type="text" name="ref-3-1" placeholder="Nom de l'entreprise">
						</div>
						<div class="col-sm-3">
							<h4>Référence 2</h4>
							<input type="text" name="ref-1-2" placeholder="Nom de l'entreprise">
							<input type="text" name="ref-2-2" placeholder="Nom de l'entreprise">
							<input type="text" name="ref-3-2" placeholder="Nom de l'entreprise">
						</div>
						<div class="col-sm-3">
							<h4>Référence 3</h4>
							<input type="text" name="ref-1-3" placeholder="Nom de l'entreprise">
							<input type="text" name="ref-2-3" placeholder="Nom de l'entreprise">
							<input type="text" name="ref-3-3" placeholder="Nom de l'entreprise">
						</div>
					</div>
					<label for="exp-description">Description</label>
					<textarea name="exp-description" id="exp-description" rows="8" cols="40" placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris."></textarea>


				</form>-->

			<form class="experiences-formu" action="#" method="post">
					<div class="experiences-form cf">
						<div class="col-sm-3 form-titles"><h4>Spécialité</h4></div>
						<div class="col-sm-3 form-titles"><h4>Référence 1</h4></div>
						<div class="col-sm-3 form-titles"><h4>Référence 2</h4></div>
						<div class="col-sm-3 form-titles"><h4>Référence 3</h4></div>

						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Spécialité"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 1"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 2"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 3"></div>

						<div class="separator-exp"></div>

						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Spécialité"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 1"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 2"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 3"></div>

						<div class="separator-exp"></div>

						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Spécialité"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 1"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 2"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 3"></div>


					</div>

					<div class="add-more">
						<div class="add-cont">
							<i class="fa fa-plus fa-5x"></i><a href="#">Rajouter</a>
						</div>
					</div>
					<label for="exp-description">Description</label>
					<textarea name="exp-description" id="exp-description" rows="8" cols="40" placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris."></textarea>


				</form>

			</div>
		</div><!-- METIER CONTAINER -->

		<div class="separator-exp"></div>

		<div class="metier-container">
			<div class="row">
				<div class="col-sm-6 name">
					<h4>Metier 1</h4>
					<p class="selected-job">
						Coach
					</p>
				</div>

				<div class="col-sm-6 years">

					<h4>Années d'expériences</h4>
					<ul>
						<li>Junior</li>
						<li class="y">10 ans</li>
						<li>Senior</li>
					</ul>
					<input type="range" name="years" value="10" max="30" min="0">
					<div class="lessmore">
						<a href="#" class="icon-less fa fa-minus"></a>
						<a href="#" class="icon-more fa fa-plus"></a>
					</div>

				</div>
			</div>

			<div class="row row-form">

			<form class="experiences-formu" action="#" method="post">
					<div class="experiences-form cf">
						<div class="col-sm-3 form-titles"><h4>Spécialité</h4></div>
						<div class="col-sm-3 form-titles"><h4>Référence 1</h4></div>
						<div class="col-sm-3 form-titles"><h4>Référence 2</h4></div>
						<div class="col-sm-3 form-titles"><h4>Référence 3</h4></div>

						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Spécialité"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 1"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 2"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 3"></div>

						<div class="separator-exp"></div>

						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Spécialité"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 1"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 2"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 3"></div>

						<div class="separator-exp"></div>

						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Spécialité"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 1"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 2"></div>
						<div class="col-sm-3"><input type="text" name="speciality-1" placeholder="Référence 3"></div>


					</div>

					<div class="add-more">
						<div class="add-cont">
							<i class="fa fa-plus fa-5x"></i><a href="#">Rajouter</a>
						</div>
					</div>
					<label for="exp-description">Description</label>
					<textarea name="exp-description" id="exp-description" rows="8" cols="40" placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris."></textarea>


				</form>
				<div class="nav-btns col-sm-12 cf">
					<button type="button" name="button" class="btn-pages">Retour</button>
					<button type="button" name="button" class="btn-pages">Suivant</button>
				</div>
			</div><!--ROW-->
		</div><!-- METIER CONTAINER -->


	</section>
</div><!-- container -->
<?php include 'includes/footer.php' ?>
