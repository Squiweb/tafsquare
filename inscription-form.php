<!-- Header -->
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>

<header class="small_header light_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered ">
			<div class="col-sm-12 col-centered">
				<h1>Une inscription
					<span>et bientôt de nouveaux contrats</span>
				</h1>
			</div>
		</div>
	</div>

</header>


<!-- INSCRIPTION NAV -->
<nav class="nav-inscription cf">
	<a href="inscription-form.php" class="active col-sm-4 col-xs-12">1. Vos coordonnées</a>
	<a href="inscription-metiers.php" class="col-sm-4 col-xs-12">2. Vos métiers</a>
	<a href="inscription-exp.php" class="col-sm-4 col-xs-12">3. Votre expérience</a>
</nav>
<!-- FORM CONTAINER -->
<div class="container">
	<form class="inscription-form  inscription-container" action="#" method="post">
		<div class="row">
			<div class="form-input col-sm-6 field-group">
				<label for="tva-input">Entrez votre numéro de TVA ou numéro d'entreprise <span class="asterix">*</span></label>
				<input type="text" name="tva" value="" placeholder="(9 ou 10 chiffres)" id="tva-input">
				<div class="tva-icon fa fa-map-marker form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="col-sm-6 field-group">
				<label for="lang-select">Langue <span class="asterix">*</span></label>
				<select class="lang_dropdown" name="lang-select" id="lang-select">
					<option value="Fr">Français</option>
					<option value="EN">Anglais</option>
					<option value="NL">Néerlandais</option>
				</select>
				<div class="tva-icon fa fa-sort-desc form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="form-input col-sm-6 field-group">
				<label for="society">Nom de la société <span class="asterix">*</span></label>
				<input type="text" name="societe" value="" placeholder="Votre nom commercial" id="society">
				<div class="tva-icon fa fa-user form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="col-sm-6 field-group">
				<div class="form-input">
					<label for="postale">Adresse postale <span class="asterix">*</span></label>
					<input type="text" name="adresse" value="" placeholder="Votre adresse" id="postale">
					<div class="tva-icon fa fa-home form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
				</div>
			</div>

			<div class="form-input col-sm-6 field-group">
				<label for="nom">Nom <span class="asterix">*</span></label>
				<input type="text" name="nom" value="" placeholder="Votre nom" id="nom">
				<div class="tva-icon fa fa-user form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="form-input col-sm-6 field-group">
				<label for="gsm">Numéro de GSM <span class="asterix">*</span></label>
				<input type="text" name="gsm" value="" placeholder="Votre numéro de gsm" id="gsm">
				<div class="tva-icon fa fa-mobile form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="form-input col-sm-6 field-group">
				<label for="prenom">Prénom <span class="asterix">*</span></label>
				<input type="text" name="prenom" value="" placeholder="Votre prénom" id="prenom">
				<div class="tva-icon fa fa-user form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="form-input col-sm-6 field-group">
				<label for="phone">Nom numéro de téléphone <span class="asterix">*</span></label>
				<input type="text" name="phone" value="" placeholder="Votre numéro de téléphone" id="phone">
				<div class="tva-icon fa fa-phone form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="form-input col-sm-6 field-group">
				<label for="web">Site web <span class="asterix">*</span></label>
				<input type="text" name="siteweb" value="" placeholder="Votre site web" id="web">
				<div class="tva-icon fa fa-globe form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>
		</div>

		<div class="row">
			<fieldset>
				<div class="form-input col-sm-6 field-group mr">
					<input type="radio" name="gender" value="" id="mr">
					<label for="mr"><span></span>Mr.</label>

				</div>
				<div class="form-input col-sm-6 field-group mdme">
					<input type="radio" name="gender" value="" id="mdme">
					<label for="mdme"><span></span>Mdme.</label>

				</div>
			</fieldset>
		</div>

		<div class="row">
			<div class="form-input col-sm-6 field-group">
				<label for="email">E-mail</label>
				<input type="text" name="tva" value="" placeholder="Votre email" id="email">
				<div class="tva-icon fa fa-envelope form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="form-input col-sm-6 field-group">
				<label for="confirm-email">Confirmer E-mail</label>
				<input type="text" name="tva" value="" placeholder="Votre email" id="confirm-email">
				<div class="tva-icon fa fa-envelope form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="form-input col-sm-6 field-group">
				<label for="psw">Mot de passe</label>
				<input type="password" name="tva" value="" placeholder="Votre mot de passe" id="psw">
				<div class="tva-icon fa fa-lock form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
			</div>

			<div class="form-input col-sm-6 field-group">
				<label for="confirm-psw">Confirmer mot de passe</label>
				<input type="text" name="tva" value="" placeholder="Votre mot de passe" id="confirm-psw">
				<div class="tva-icon fa fa-lock form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
				<span class="note">*mention obligatoire</span>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-8 field-group">
				<input type="checkbox" name="cgu" value="true" id="cgu">
				<label for="cgu"  class="checkbox-label"><span></span>J'ai lu et j'accepte les <a href="#">conditions générales</a> d'utilisation de ce site internet.</label>
			</div>
			<div class="col-sm-8 field-group">
				<input type="checkbox" name="newsletter" id="newsletter" value="true">
				<label for="newsletter" class="checkbox-label"><span></span>Je souhaite recevoir les actus Tafsquare par email</label>
			</div>
		</div>
	</form>
	<div class="nav-btns">
		<button type="button" name="button" class="btn-pages">Suivant</button>
	</div>
</div><!--CONTAINER -->
<?php include 'includes/footer.php' ?>
