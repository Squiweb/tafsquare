<!-- Header -->
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>
<header class="small_header light_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>
	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h2>Un nouveau métier ?</h2>
			</div>
		</div>
	</div>
</header>

<!-- TVA CONTAINER -->
<div class="container">
	<div class="row cf">
		<form class="inscription-form inscription-container new-taf " action="#" method="post">

			<div class="inputs cf">
				<div class="form-input col-sm-5 field-group">
					<label for="email">Email</label>
					<input type="text" name="email" value="" placeholder="Votre email" id="email">
					<div class="tva-icon fa fa-envelope form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
				</div>
				<div class="form-input col-sm-5 field-group">
					<label for="email">Métier</label>
					<input type="text" name="newtaf" value="" placeholder="Indiquez le nom du métier" id="newtaf" class="newtaf-icon">
					<div class="tva-icon fa fa-briefcase form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
				</div>
			</div>
			<label for="exp-description" class="motivation">Motivation de la demande</label>
			<textarea name="exp-description" id="exp-description" rows="8" cols="40" placeholder="Indiquez-nous les raisons pour lesquelles vous souhaitez soumettre ce nouveau métier"></textarea>
		</form>
		<div class="col-sm-12">
			<div class="nav-btns cf">
				<button type="button" name="button" class="btn-pages">Retour</button>
				<button type="button" name="button" class="btn-pages">Valider</button>
			</div>
		</div>
	</div><!--row-->
</div><!--container-->
<?php include 'includes/footer.php' ?>
