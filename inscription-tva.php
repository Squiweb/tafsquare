<!-- Header -->
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>


<header class="small_header light_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1>Une inscription
					<span>et bientôt de nouveaux contrats</span>
				</h1>
			</div>
		</div>
	</div>

</header>


<!-- INSCRIPTION NAV -->
<nav class="nav-inscription cf">
	<a href="inscription-form.php" class="active col-sm-4 col-xs-12">1. Vos coordonnées</a>
	<a href="inscription-metiers.php" class="col-sm-4 col-xs-12">2. Vos métiers</a>
	<a href="inscription-exp.php" class="col-sm-4 col-xs-12">3. Votre expérience</a>
</nav>


<!-- TVA CONTAINER -->
<div class="container">
	<div class="row">
		<div class="tva inscription-container">
			<form class="inscription-form  inscription-container container" action="#" method="post">

				<div class="form-input col-sm-8 col-sm-offset-2 field-group">
					<label for="tva-input">Entrez votre numéro de TVA ou numéro d'entreprise <span class="asterix">*</span></label>
					<input type="text" name="tva" value="" placeholder="(9 ou 10 chiffres)" id="tva-input">
					<div class="tva-icon fa fa-map-marker form-icon"><span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>

				</div>
			</form>
		</div>
	</div>
</div>
<?php include 'includes/footer.php' ?>
