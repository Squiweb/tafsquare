/*

  Author: Supview
  Version: 1.0
  URL: #

*/




jQuery(document).ready(function () { // Document ready

  "use strict";
  PointerEventsPolyfill.initialize({});
  autosize($('textarea'));

    /*-----------------------------------------------------------------------------------*/
    /*	02. STICKY OUR NAV
    /*-----------------------------------------------------------------------------------*/

  		/*$('#principal').stick_in_parent({
  			offset_top: 60,
  		});

      $("nav").waypoint(function(direction) {
              // Scrolling down
              if (direction === 'down') {
                  $(document.body).trigger("sticky_kit:recalc");
              } else {
              }
      }, { offset: "0px" });+/


    /*-----------------------------------------------------------------------------------*/
    /*	03. MAKEITSWIPE TAF
    /*-----------------------------------------------------------------------------------*/


    $(".swipe-taf").owlCarousel({
        slideSpeed: 800,
        autoplay:true,
        autoplayTimeout:1000,
        items: 4,
        autoHeight: true,
        navigation: true,
        navigationText: [
        "<i class='fa fa-angle-left'></i>",
        "<i class='fa fa-angle-right'></i>"
        ],
        transitionStyle: "fade",
    });


    /*-----------------------------------------------------------------------------------*/
    /*	04. MAKEITSWIPE TESTIMONIALS
    /*-----------------------------------------------------------------------------------*/


    $(".makeitswipe").owlCarousel({
        slideSpeed: 800,
        autoplay:true,
        autoplayTimeout:1000,
        singleItem: true,
        autoHeight: true,
        navigation: true,
        navigationText: [
        "<i class='fa fa-angle-left'></i>",
        "<i class='fa fa-angle-right'></i>"
        ],
        transitionStyle: "fade",
    });

    /*-----------------------------------------------------------------------------------*/
    /*	05. PANEL TOP
    /*-----------------------------------------------------------------------------------*/


    var tl = new TimelineLite();

    tl.to($('#first_panel'), 0 , { scale:1, marginTop: -640}, 0)
    tl.to($('#first_panel').find('.close-btn'), 0 , { autoAlpha: 0}, 0)

    $('.bandeau .close-btn').click(function() {
      var tl = new TimelineLite();
      tl.to($('#first_panel'), 0.7 , { scale:1, marginTop: -640}, 0)
      tl.to($('#first_panel').find('.close-btn'), 0.4 , { autoAlpha: 0}, 0.4)
      tl.to($('#principal'), 0.7 , { scale:1, marginTop: 0}, 0)
      tl.to($('.whole-container'), 0.7 , { scale:1, marginTop: 0}, 0)
      $('.bandeau .btn-bord').removeClass('clicked');
    });

    $('.bandeau .btn-bord').click(function() {
      var tl = new TimelineLite();
      tl.to($('#first_panel'), 0.7 , { scale:1, marginTop: 0}, 0)
      tl.to($('#first_panel').find('.close-btn'), 0.4 , { autoAlpha: 1}, 0)
      tl.to($('#principal'), 0.7 , { scale:1, marginTop: 640}, 0)
      tl.to($('.whole-container'), 0.7 , { scale:1, marginTop: 640}, 0)
    });


    $('.bandeau .btn-bord').click(function(){
        if($(this).hasClass('clicked')){
            $(this).removeClass('clicked');
            var tl = new TimelineLite();
            tl.to($('#first_panel'), 0.7 , { scale:1, marginTop: -640}, 0)
            tl.to($('#first_panel').find('.close-btn'), 0.4 , { autoAlpha: 0}, 0.4)
            tl.to($('#principal'), 0.7 , { scale:1, marginTop: 0}, 0)
            tl.to($('.whole-container'), 0.7 , { scale:1, marginTop: 0}, 0)
        } else {
            $(this).addClass('clicked');
            var tl = new TimelineLite();
            tl.to($('#first_panel'), 0.7 , { scale:1, marginTop: 0}, 0)
            tl.to($('#first_panel').find('.close-btn'), 0.4 , { autoAlpha: 1}, 0)
            tl.to($('#principal'), 0.7 , { scale:1, marginTop: 640}, 0)
            tl.to($('.whole-container'), 0.7 , { scale:1, marginTop: 640}, 0)
        }
    });


    /*-----------------------------------------------------------------------------------*/
    /*	06. INVIEW EFFECTS
    /*-----------------------------------------------------------------------------------*/

    /*tl.to($('#latest_taf').find('li'), 0 , {autoAlpha: 0}, 0)
    var inview = new Waypoint.Inview({
      element: $('#latest_taf')[0],
      enter: function(direction) {
        tl.staggerTo($('#latest_taf').find('li'), 0.4 , {autoAlpha: 1}, 0.4)
      }
    })

    tl.to($('#whyuse').find('li'), 0 , {autoAlpha: 0}, 0)
    var inview = new Waypoint.Inview({
      element: $('#latest_taf')[0],
      enter: function(direction) {
        tl.staggerTo($('#whyuse').find('li'), 0.4 , {autoAlpha: 1}, 0.4)
      }
    })
*/
  /*-----------------------------------------------------------------------------------*/
  /*	07. NAVIGATION SHOW HIDE
  /*-----------------------------------------------------------------------------------*/

  	$(".navToggle").on("click", function(){

  		  $(this).toggleClass("open");

  		  $("#principal .principal_nav").toggleClass("active");

    });


  /*-----------------------------------------------------------------------------------*/
  /*	08. COOKIES ACCEPT
  /*-----------------------------------------------------------------------------------*/
    //
    //
    //
    // tl.to($('.cookie-bar'), 0 , { scale:1, bottom: -80}, 0)
    // tl.to($('.cookie-bar'), 0.4 , { scale:1, bottom: 0}, 0.6)
    //
    $(".btn-cookies").on("click", function(e){
        e.preventDefault();
        $('.cookie-bar').addClass('cookies-hide');
    });
    /*-----------------------------------------------------------------------------------*/
    /*	08. COOKIES ACCEPT
    /*-----------------------------------------------------------------------------------*/
      //
      //
      //
      // tl.to($('.cookie-bar'), 0 , { scale:1, bottom: -80}, 0)
      // tl.to($('.cookie-bar'), 0.4 , { scale:1, bottom: 0}, 0.6)
      //
      $(".close-btn-mobile").on("click", function(e){
          e.preventDefault();
          $('.totranslate').addClass('translated');
      });


}); /* END OF Document Ready */
