<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>
<header class="small_header light_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1>Merci</h1>
			</div>
		</div>
	</div>

</header>


<section class="message">
  <div class="container">
		<div class="row row_logo">
      <div class="background_logo col-sm-4">
        <img src="img/logo-bg.png" alt="Logo Tafsquare">
      </div>
      <div class="col-sm-8 confirmation">
        <h2>Merci !</h2>
        <p>Nous avons bien enregistré votre désintérêt pour ce TAF. Ca sera peut-être pour une prochaine fois !</p><br>
        <p>Néanmoins, si vous voulez recevoir encore plus de TAFs gratuitement, merci de vous inscrire.</p>

        <div class="separator"></div>

        <h3>Pas encore membre ?</h3>
        <a class="simplelink" href="#">Incrivez-vous sur tafsquare</a>

      </div>


    </div>
  </div>
</section>

<?php include 'includes/footer.php' ?>
