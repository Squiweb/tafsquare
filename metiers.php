<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>
<header class="small_header light_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1>Métiers</h1>
			</div>
		</div>
	</div>

</header>

<section id="metiers_domains">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-centered">
        <h2>Quel est votre domaine ?</h2>
        <p class="lead">145 domaines d’activité, bien rangés en 6 catégories</p>
      </div>

      <ul class="three_up tiles grille-domaines">

        <li class="domaine">
          <div class="bg"><img src="https://images.unsplash.com/photo-1447752875215-b2761acb3c5d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=e18a8e9c0b879cfb054cd6ee89ecf803" alt="exemple"></div>
          <h3 class="verticalalign">Immobilier</h3>
					<a href="#" class="banner_link"></a>
        </li>

        <li class="domaine">
          <div class="bg"><img src="https://images.unsplash.com/photo-1419312520378-cbd583837112?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=f2ab4f40f9d5a6cee0fcab65cba5c325" alt="exemple"></div>
          <h3 class="verticalalign">Santé</h3>
					<a href="#" class="banner_link"></a>
        </li>

        <li class="domaine">
          <div class="bg"><img src="https://images.unsplash.com/photo-1449265614232-03dfc33163a0?crop=entropy&fit=crop&fm=jpg&h=975&ixjsv=2.1.0&ixlib=rb-0.3.5&q=80&w=2125" alt="exemple"></div>
          <h3 class="verticalalign">Culture & Divertissement</h3>
					<a href="#" class="banner_link"></a>
        </li>

        <li class="domaine">
          <div class="bg"><img src="https://images.unsplash.com/photo-1414690165279-49ab0a9a7e66?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=d36ea3a39c51d6711a4ccbd1d37124b1" alt="exemple"></div>
          <h3 class="verticalalign">Web & Technologies</h3>
					<a href="#" class="banner_link"></a>
        </li>

        <li class="domaine">
          <div class="bg"><img src="https://images.unsplash.com/photo-1430651717504-ebb9e3e6795e?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=1850d66a67f2a46276a0bad9ae209581" alt="exemple"></div>
          <h3 class="verticalalign">Économie</h3>
					<a href="#" class="banner_link"></a>
        </li>

        <li class="domaine">
          <div class="bg"><img src="https://images.unsplash.com/36/yJl7OB3sSpOdEIpHhZhd_DSC_1929_1.jpg?crop=entropy&fit=crop&fm=jpg&h=975&ixjsv=2.1.0&ixlib=rb-0.3.5&q=80&w=2125" alt="exemple"></div>
          <h3 class="verticalalign">Commerce</h3>
					<a href="#" class="banner_link"></a>
        </li>

				<li class="domaine semi">
          <div class="bg"><img src="https://images.unsplash.com/photo-1447752875215-b2761acb3c5d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=e18a8e9c0b879cfb054cd6ee89ecf803" alt="exemple"></div>
          <h3 class="verticalalign">Immobilier</h3>
					<a href="#" class="banner_link"></a>
        </li>

				<li class="domaine semi">
          <div class="bg"><img src="https://images.unsplash.com/photo-1447752875215-b2761acb3c5d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=e18a8e9c0b879cfb054cd6ee89ecf803" alt="exemple"></div>
          <h3 class="verticalalign">Immobilier</h3>
					<a href="#" class="banner_link"></a>
        </li>

				<li class="domaine full">
          <div class="bg"><img src="https://images.unsplash.com/photo-1447752875215-b2761acb3c5d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=e18a8e9c0b879cfb054cd6ee89ecf803" alt="exemple"></div>
          <h3 class="verticalalign">Immobilier</h3>
					<a href="#" class="banner_link"></a>
        </li>

      </ul>

      <div class="col-md-7 info-text">

        <h2 class="metier-title"><img src="img/icon/icons-01.png" class="metier-icon">Interessé pour créer un TAF sit amet, consectetur adipis- cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</h3>
      </div>

      <div class="slice">

        <div class="col-md-4 sub_metiers">
          <h3>Bâtiment & construction</h3>
          <ul class="list-check">
            <li><a href="#">Géomètre</a></li>
            <li><a href="#">Entrepreneur / Maçon</a></li>
            <li><a href="#">Peintre</a></li>
            <li><a href="#">Plafonneur</a></li>
            <li><a href="#">Carreleur</a></li>
            <li><a href="#">Plombier</a></li>
            <li><a href="#">Chauffagiste</a></li>
            <li><a href="#">Eléctricien</a></li>
            <li><a href="#">Charpentier</a></li>
            <li><a href="#">Menuisier</a></li>
            <li><a href="#">Terrassier</a></li>
            <li><a href="#">Couvreur/couvreuse</a></li>
            <li><a href="#">Etancheur</a></li>
            <li><a href="#">Entreprise d isolation</a></li>
            <li><a href="#">Ingénieur</a></li>
          </ul>
        </div>

        <div class="col-md-4 sub_metiers">
          <h3>Sécurité & entretien</h3>
          <ul class="list-check">
            <li><a href="#">Société de nettoyage & assainissement</a></li>
            <li><a href="#">Société de protection & surveillance</a></li>
          </ul>
        </div>

        <div class="col-md-4 sub_metiers">
          <h3>Art du bâtiment</h3>
          <ul class="list-check">
            <li><a href="#">Architecte</a></li>
            <li><a href="#">Architecte d'intérieur</a></li>
            <li><a href="#">Décorateur/décoratrice d'intérieur</a></li>
            <li><a href="#">Jardinier</a></li>
            <li><a href="#">Vitrier</a></li>
            <li><a href="#">Restaurateur de bâtiments</a></li>
            <li><a href="#">Commerçant d articles de sport</a></li>
          </ul>
        </div>

      </div>


			<div class="col-sm-12 nav-btns cf">
				<a href="#" class="btn-pages">Retour</a>
			</div>
    </div>
  </div>
</section>


<?php include 'includes/footer.php' ?>
