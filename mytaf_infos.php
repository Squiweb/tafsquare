<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>
<header class="small_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1 class="asterix">Charpentier</h1>
			</div>
		</div>
	</div>

</header>

<div class="inner_taf_info mytaf_infos">
	<div class="container">
		<div class="row">
			<div class="interet_nombre col-md-3">
				<p>Actuellement</p>
				<span>5</span>
				<p>indépendants ont<br> montré leur intérêt</p>
				<!--<a class="btn-rouge btn-status" href="#">￼Cliquez ici pour visualiser<br> leurs coordonées</a>-->
				<a class="btn-yellow btn-status" href="#">￼En cours de validation</a>
				<a class="btn-vert btn-status" href="#">￼Modifier le taf</a>
				<!--<a class="btn-rouge btn-status" href="#">￼Payer</a>-->
				<div class="supprimer" data-toggle="modal" data-target="#supprimer">Supprimer</div>
			</div>

			<div class="price col-md-4">
				<span class="le_prix">Budget : 1500€ htva</span>
				<div class="col-md-12 price_info">
					<i class="fa fa-map-marker"></i> Charleroi
				</div>
				<div class="col-md-12 price_info">
					<i class="fa fa-clock-o"></i>du 4 janv. au 12 janv. 2016
				</div>
			</div>
		</div>
	</div>
</div>





<div class="inner_taf">

	<div class="container">

			<div class="col-md-12 titre">
				<h2>Titre:</h2>
				<p>Cherche charpentier pour ma maison</p>
			</div>

			<div class="col-md-12 description">
				<h2>Description:</h2>
				<p>
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo- do consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
					Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
				</p>
			</div>

			<div class="col-md-12 coordonnees">
				<h3>Coordonnées des pros qui ont montré un intêret à votre TAF :</h3>
			</div>
			<section class="col-md-12 filters">
				<div class="tri cf">
					<p>Trier par :</p>
					<ul>
						<li><a href="#" class="active">Le plus récent</a></li>
						<li><a href="#">Score</a></li>
						<li><a href="#">Expérience</a></li>
					</ul>
				</div>

				<div class="block-pagination">
					<div class="group"><p>Eléments affichés :</p>
						<select class="affichage elements" name="affichage" id="affichage">
							<option value="10">10</option>
							<option value="10">20</option>
							<option value="10">30</option>
							<option value="10">40</option>
						</select></div>

						<ul class="pagination">
							<li>
								<a href="#" aria-label="Previous">
									<span aria-hidden="true">«</span>
								</a>
							</li>
							<li>
								<a href="#" class="active">1</a>
							</li>
							<li>
								<a href="#">2</a>
							</li>
							<li>
								<a href="#" aria-label="Previous">
									<span aria-hidden="true">»</span>
								</a>
							</li>
						</ul>
					</div>

				</section>
				<div class="col-md-12 coordonnees">
					<!-- TITRE DU PANEL -->
					<div class="titre_coordonnee pro_selected">
						<div class="row">
							<div class="col-md-3">
								<h4>Société A</h4>
								<h5>Robert Dutoit</h5>
							</div>
							<div class="col-md-2 lieu">
								<p><i class="fa fa-map-marker"></i> Florennes</p>
							</div>

							<div class="col-md-3 experiences">
								<p>Expérience: <span>10</span> ans</p>
							</div>

							<div class="col-md-4 inner-score">
								<div class="score-rate">Score actuel :
									<div class="rating-stars rating-infos">
										<span class="fa fa-star asterix"></span>
										<span class="fa fa-star asterix"></span>
										<span class="fa fa-star asterix"></span>
										<span class="fa fa-star asterix"></span>
				            <span class="fa fa-star"></span>
				          </div>
								</div>
							</div>
						</div>
					</div>
					<!-- PANEL -->
					<div class="panel-coordonnee">

						<!-- BLOCK DE COORDONNEE -->
						<div class="block_coordonnee">

							<div class="row">
							<div class="col-md-8">
								<p class="date-interet">... a marqué son intérêt le 25 décembre 2015</p>
							</div>

							<div class="col-md-4 inner-score">
								<div class="score">
							<a href="#">12 avis</a><br>
							<a href="#">Evaluez ce professionnel</a>
						</div>
				</div>
			</div>

			<div class="row">
				<div class="separator"></div>
				<div class="col-md-12">
					<p class="lead">Contact:</p>
				</div>
				<div class="col-md-4">
					<p><strong>Tél:</strong> <a href="#">071 33 53 43</a></p>
					<p><strong>GSM:</strong> <a href="#">0472 72 72 73</a></p>
				</div>
				<div class="col-md-4">
					<p><strong>Email:</strong> <a href="#">totto@gmail.com</a></p>
					<p><strong>Site internet:</strong> <a href="#">www.exemple.be</a></p>
				</div>
			</div>

			<div class="row">
				<div class="separator"></div>

				<div class="col-md-3 md-max-hide">
					<p class="lead">Spécialités</p>
				</div>
				<div class="col-md-3 md-max-hide">
					<p class="lead">Référence 1</p>
				</div>
				<div class="col-md-3 md-max-hide">
					<p class="lead">Référence 2</p>
				</div>
				<div class="col-md-3 md-max-hide">
					<p class="lead">Référence 3</p>
				</div>

				<div class="col-md-3 margin-top-m-15">
					<p class="input-like">Spécialité</p>
				</div>
				<div class="col-md-3">
					<p class="input-like">Nom de l’entreprise 1</p>
				</div>
				<div class="col-md-3">
					<p class="input-like">Nom de l’entreprise 2</p>
				</div>
				<div class="col-md-3">
					<p class="input-like">Nom de l’entreprise 3</p>
				</div>

				<div class="col-md-3 margin-top-m-15">
					<p class="input-like">Spécialité</p>
				</div>
				<div class="col-md-3">
					<p class="input-like">Nom de l’entreprise 1</p>
				</div>
				<div class="col-md-3">
					<p class="input-like">Nom de l’entreprise 2</p>
				</div>
				<div class="col-md-3">
					<p class="input-like">Nom de l’entreprise 3</p>
				</div>

				<div class="col-md-3 margin-top-m-15">
					<p class="input-like">Spécialité</p>
				</div>
				<div class="col-md-3">
					<p class="input-like">Nom de l’entreprise 1</p>
				</div>
				<div class="col-md-3">
					<p class="input-like">Nom de l’entreprise 2</p>
				</div>
				<div class="col-md-3">
					<p class="input-like">Nom de l’entreprise 3</p>
				</div>



				<div class="col-md-12 description">

					<p class="lead">Description</p>
					<p class="input-like">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				</div>

				<div class="col-md-12 description">
					<p class="lead">Métier(s):</p>
				</div>

				<div class="col-md-8 les-metiers">
					<strong><a href="#">Charpentier</a></strong>
					<a href="#">Menuisier</a>
					<a href="#">Architecte</a>
				</div>

				<div class="col-md-4 text-right">
					<button type="button" class="btn-rouge">Annuler le choix du prestataire</button>
				</div>

			</div>

		</div> <!-- END BLOCK DE COORDONNEE -->
	</div>  <!-- END PANEL -->

	<!-- TITRE DU PANEL -->
	<div class="titre_coordonnee even">
		<div class="row">
			<div class="col-md-3">
				<h4>Société B</h4>
				<h5>Rochez Benjamin</h5>
			</div>
			<div class="col-md-2 lieu">
				<p><i class="fa fa-map-marker"></i> Mons</p>
			</div>

			<div class="col-md-3 experiences">
				<p>Expérience: <span>157</span> ans</p>
			</div>

			<div class="col-md-4 inner-score">
				<div class="score-rate">Score actuel :
					<div class="rating-stars rating-infos">
						<span class="fa fa-star asterix"></span>
						<span class="fa fa-star asterix"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- PANEL -->
	<div class="panel-coordonnee">

		<!-- BLOCK DE COORDONNEE -->
		<div class="block_coordonnee">

			<div class="row">
				<!--	<div class="col-md-3">
				<h4>Société A</h4>
				<h5>Robert Dutoit</h5>
			</div>-->
			<div class="col-md-8">
				<!--<p class="lieu"><i class="fa fa-map-marker"></i> Florennes</p>-->
				<p class="date-interet">... a marqué son intérêt le 25 décembre 2015</p>
			</div>

			<div class="col-md-4 inner-score">
				<div class="score">
					<!--<div class="score-rate">Score actuel :
					<div class="ratings">
					<span class="asterix">★</span><span class="asterix">★</span><span class="asterix">★</span><span class="asterix">★</span><span>★</span>
				</div>
			</div>-->
			<a href="#">12 avis</a><br>
			<a href="#">Evaluez ce professionnel</a>
		</div>
		<!--<div class="experience-block">
		<p>Expérience: <span>10</span> ans</p>
	</div>-->
</div>
</div>

<div class="row">
	<div class="separator"></div>
	<div class="col-md-12">
		<p class="lead">Contact:</p>
	</div>
	<div class="col-md-4">
		<p><strong>Tél:</strong> <a href="#">071 33 53 43</a></p>
		<p><strong>GSM:</strong> <a href="#">0472 72 72 73</a></p>
	</div>
	<div class="col-md-4">
		<p><strong>Email:</strong> <a href="#">totto@gmail.com</a></p>
		<p><strong>Site internet:</strong> <a href="#">www.exemple.be</a></p>
	</div>
</div>

<div class="row">
	<div class="separator"></div>
	<div class="col-md-3 md-max-hide">
		<p class="lead">Spécialités</p>
	</div>
	<div class="col-md-3 md-max-hide">
		<p class="lead">Référence 1</p>
	</div>
	<div class="col-md-3 md-max-hide">
		<p class="lead">Référence 2</p>
	</div>
	<div class="col-md-3 md-max-hide">
		<p class="lead">Référence 3</p>
	</div>

	<div class="col-md-3 margin-top-m-15">
		<p class="input-like">Spécialité</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 1</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 2</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 3</p>
	</div>

	<div class="col-md-3 margin-top-m-15">
		<p class="input-like">Spécialité</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 1</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 2</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 3</p>
	</div>

	<div class="col-md-3 margin-top-m-15">
		<p class="input-like">Spécialité</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 1</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 2</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 3</p>
	</div>

	<div class="col-md-12 description">

		<p class="lead">Description</p>
		<p class="input-like">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

	</div>

	<div class="col-md-12 description">
		<p class="lead">Métier(s):</p>
	</div>

	<div class="col-md-8 les-metiers">
		<strong><a href="#">Charpentier</a></strong>
		<a href="#">Menuisier</a>
		<a href="#">Architecte</a>
	</div>

	<div class="col-md-4 text-right">
		<button type="button" class="btn-pages">Choisir ce prestataire</button>
	</div>

</div>

</div> <!-- END BLOCK DE COORDONNEE -->
</div>  <!-- END PANEL -->
<!-- TITRE DU PANEL -->
<div class="titre_coordonnee">
	<div class="row">
		<div class="col-md-3">
			<h4>Société C</h4>
			<h5>Thibault Cyriaque</h5>
		</div>
		<div class="col-md-2 lieu">
			<p><i class="fa fa-map-marker"></i> Bonche</p>
		</div>

		<div class="col-md-3 experiences">
			<p>Expérience: <span>3</span> ans</p>
		</div>

		<div class="col-md-4 inner-score">
			<div class="score-rate">Score actuel :
				<div class="rating-stars rating-infos">
					<span class="fa fa-star asterix"></span>
					<span class="fa fa-star asterix"></span>
					<span class="fa fa-star asterix"></span>
					<span class="fa fa-star asterix"></span>
					<span class="fa fa-star asterix"></span>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- PANEL -->
<div class="panel-coordonnee">

	<!-- BLOCK DE COORDONNEE -->
	<div class="block_coordonnee">

		<div class="row">
			<!--	<div class="col-md-3">
			<h4>Société A</h4>
			<h5>Robert Dutoit</h5>
		</div>-->
		<div class="col-md-8">
			<!--<p class="lieu"><i class="fa fa-map-marker"></i> Florennes</p>-->
			<p class="date-interet">... a marqué son intérêt le 25 décembre 2015</p>
		</div>

		<div class="col-md-4 inner-score">
			<div class="score">
				<!--<div class="score-rate">Score actuel :
				<div class="ratings">
				<span class="asterix">★</span><span class="asterix">★</span><span class="asterix">★</span><span class="asterix">★</span><span>★</span>
			</div>
		</div>-->
		<a href="#">12 avis</a><br>
		<a href="#">Evaluez ce professionnel</a>
	</div>
	<!--<div class="experience-block">
	<p>Expérience: <span>10</span> ans</p>
</div>-->
</div>
</div>

<div class="row">
	<div class="separator"></div>
	<div class="col-md-12">
		<p class="lead">Contact:</p>
	</div>
	<div class="col-md-4">
		<p><strong>Tél:</strong> <a href="#">071 33 53 43</a></p>
		<p><strong>GSM:</strong> <a href="#">0472 72 72 73</a></p>
	</div>
	<div class="col-md-4">
		<p><strong>Email:</strong> <a href="#">totto@gmail.com</a></p>
		<p><strong>Site internet:</strong> <a href="#">www.exemple.be</a></p>
	</div>
</div>

<div class="row">
	<div class="separator"></div>
	<div class="col-md-3 md-max-hide">
		<p class="lead">Spécialités</p>
	</div>
	<div class="col-md-3 md-max-hide">
		<p class="lead">Référence 1</p>
	</div>
	<div class="col-md-3 md-max-hide">
		<p class="lead">Référence 2</p>
	</div>
	<div class="col-md-3 md-max-hide">
		<p class="lead">Référence 3</p>
	</div>

	<div class="col-md-3 margin-top-m-15">
		<p class="input-like">Spécialité</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 1</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 2</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 3</p>
	</div>

	<div class="col-md-3 margin-top-m-15">
		<p class="input-like">Spécialité</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 1</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 2</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 3</p>
	</div>

	<div class="col-md-3 margin-top-m-15">
		<p class="input-like">Spécialité</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 1</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 2</p>
	</div>
	<div class="col-md-3">
		<p class="input-like">Nom de l’entreprise 3</p>
	</div>©

	<div class="col-md-12 description">

		<p class="lead">Description</p>
		<p class="input-like">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

	</div>

	<div class="col-md-12 description">
		<p class="lead">Métier(s):</p>
	</div>

	<div class="col-md-8 les-metiers">
		<strong><a href="#">Charpentier</a></strong>
		<a href="#">Menuisier</a>
		<a href="#">Architecte</a>
	</div>

	<div class="col-md-4 text-right">
		<button type="button" class="btn-pages">Choisir ce prestataire</button>
	</div>

</div>

</div> <!-- END BLOCK DE COORDONNEE -->
</div>  <!-- END PANEL -->


<!-- TITRE DU PANEL -->

</div>


<div class="col-sm-12 nav-btns cf " >
	<button type="button" name="button" class="btn-pages">Retour</button>
</div>

</div><!--container-->

</div><!--INNER-->




<?php include 'includes/footer.php' ?>


<script>

/*-----------------------------------------------------------------------------------*/
/*	09. PRESTATAIRES PANELS
/*-----------------------------------------------------------------------------------*/

$('.panel-coordonnee').hide();
$('.titre_coordonnee').click(function(){
	$(".panel-coordonnee").not($(this).next()).slideUp(800);
	$(this).next().slideToggle(1200);
});

</script>
