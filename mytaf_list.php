<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>


<header class="small_header show_all_tafs_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>
	<div class="container vertical_align">
		<form class="inscription-form filters_header mytaf_list_header row" action="#" method="post">
			<div class="field-group col-sm-2 col-sm-offset-2">
				<label for="cities">Métier</label>
				<input type="text" name="name" placeholder="Je recherche">
			</div>
			<div class="field-group col-sm-2">
				<label for="cities">Zone géographique</label>
				<select name="cities" id="cities">
					<option value="all">Zone géographique</option>
				</select>
			</div>
			<div class="field-group col-sm-2">
				<label for="domaines">Catégorie</label>
				<select name="domaines" id="domaines">
					<option value="all">Catégorie</option>
				</select>
			</div>
			<div class="col-sm-2 col-btn">
				<a href="#" class="btn-yellow">Rechercher</a>
			</div>
		</form>
	</div>

</header>

<div class="select_category">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<a class="active" href="#">Mes TAFs</a>
			</div>
			<div class="col-md-2">
				<a href="mytaf_interets.php">Mes intérêts</a>
			</div>
		</div>
	</div>
</div>

<div class="category_filter">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<input type="checkbox" name="checkbox" id="encours" value="value">
				<label for="encours">En cours</label>
				<input type="checkbox" name="checkbox" id="topay" value="value">
				<label for="topay">A payer</label>
				<input type="checkbox" name="checkbox" id="payed" value="value">
				<label for="payed">Payé</label>
				<input type="checkbox" name="checkbox" id="cloture" value="value">
				<label for="cloture">Cloturé</label>
			</div>
		</div>
	</div>
</div>

<div class="category_tafs">

	<div class="container">

    <div class="row">

			<div class="col-md-12 col-centered">

				<ul class="four_up tiles">
					<!-- TAF -->
					<li class="taf middle-time">

						<div class="hoverit">
							<h4>Voir</h4>
							<div class="status">
								<div class="modifier">Modifier</div>
								<div class="supprimer" data-toggle="modal" data-target="#supprimer">Supprimer</div>
							</div>
						</div>

						<span class="reponse toconfirm">0<br><span class="small-reponse">réponses</span><span class="status">En validation</span></span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Charpentier</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Charleroi
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 5 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf end-time">

						<div class="hoverit">
							<h4>Voir</h4>
							<div class="status">
								<div class="modifier">Modifier</div>
								<div class="supprimer" data-toggle="modal" data-target="#supprimer">Supprimer</div>
							</div>
						</div>

						<span class="reponse toconfirm">02<br><span class="small-reponse">réponses</span><span class="status">En validation</span></span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Peintre</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Charleroi
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 2 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf">

						<div class="hoverit">
							<h4>Voir</h4>
							<div class="status">
								<div class="apayer">À payer</div>
								<div class="supprimer" data-toggle="modal" data-target="#supprimer">Supprimer</div>
							</div>
						</div>

						<span class="reponse topay">02<br><span class="small-reponse">réponses</span><span class="status">A payer</span></span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Squiweb</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Bruxelles
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 14 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf end-time">

						<div class="hoverit">
							<h4>Voir</h4>
							<div class="status">
								<div class="apayer">À payer</div>
								<div class="supprimer" data-toggle="modal" data-target="#supprimer">Supprimer</div>
							</div>
						</div>

						<span class="reponse topay">02<br><span class="small-reponse">réponses</span><span class="status">A payer</span></span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Magouilleur</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Mons
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 2 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf expired">
						<span class="reponse">02<br><span class="small-reponse">réponses</span><span class="status">Expiré</span></span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Magouilleur</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Lille
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expiré</span>
					</li>
					<!-- TAF -->
					<li class="taf expired">
						<span class="reponse">02<br><span class="small-reponse">réponses</span><span class="status">Expiré</span></span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Magouilleur</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Lille
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expiré</span>
					</li>
					<!-- TAF -->
					<li class="taf">
						<span class="reponse topay">02<br><span class="small-reponse">réponses</span><span class="status">A payer</span></span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Magouilleur</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Lille
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 14 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf">
						<span class="reponse topay">02<br><span class="small-reponse">réponses</span><span class="status">A payer</span></span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Magouilleur</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Lille
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 14 jours</span>
					</li>

				</ul>

			</div>

		</div>

	</div>

</div>


<div class="block-pagination">
	<ul class="pagination">
	<li>
		<a href="#" aria-label="Previous">
			<span aria-hidden="true">&laquo;</span>
		</a>
	</li>
	<li class="active"><a href="#">1</a></li>
	<li><a href="#">2</a></li>
	<li><a href="#">3</a></li>
	<li><a href="#">4</a></li>
	<li><a href="#">5</a></li>
	<li>
		<a href="#" aria-label="Next">
			<span aria-hidden="true">&raquo;</span>
		</a>
	</li>
</ul>
</div>

<!-- TRIGGER MODALS -->
<!-- Modal -->
<div class="modal fade" id="supprimer" role="dialog">
	<div class="modal-dialog modal-suppression">
		<div class="modal-content">
			<div class="modal-body">
				<h3>Êtes-vous sûr de vouloir supprimer ce TAF?</h3>
				<div class="field-group">
					<a href="#">OUI</a>
					<a href="#">NON</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'includes/footer.php' ?>
