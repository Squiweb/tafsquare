<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>
<header class="small_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1 class="asterix">Charpentier</h1>
			</div>
		</div>
	</div>

</header>


<div class="inner_taf_info">
	<div class="container">
		<div class="interet_nombre col-md-3">
			<p>Actuellement</p>
			<span>5</span>
			<p>indépendants ont<br> montré leur intérêt</p>
			<a class="btn-rouge btn-status" href="#">￼Cliquez ici pour visualiser<br> leurs coordonées</a>
			<!--<a class="btn-yellow btn-status" href="#">￼En cours de validation</a>-->
			<!--<a class="btn-vert btn-status" href="#">￼Modifier le taf</a>-->
			<!--<a class="btn-rouge btn-status" href="#">￼Payer</a>-->
			<div class="supprimer" data-toggle="modal" data-target="#supprimer">Supprimer</div>
		</div>

		<div class="price col-md-4">
			<span class="le_prix">1500€ htva</span>
			<div class="col-md-12 price_info">
				<i class="fa fa-map-marker"></i> Charleroi
			</div>
			<div class="col-md-12 price_info">
				<i class="fa fa-clock-o"></i>du 4 janv. au 12 janv. 2016
			</div>
		</div>
	</div>
</div>


<div class="inner_taf">

	<div class="container">

    <div class="row">

			<div class="col-md-12 titre">
				<h2>Titre:</h2>
				<p>Cherche charpentier pour ma maison</p>
			</div>

			<div class="col-md-10 description">
				<h2>Description:</h2>
				<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo- do consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
			</div>

			<div class="nav-btns col-sm-12 cf">
				<button type="button" name="button" class="btn-pages">Retour</button>
			</div>

		</div><!--row-->

	</div><!--container-->

</div><!--inner-->
<!-- TRIGGER MODALS -->
<!-- Modal -->
<div class="modal fade" id="supprimer" role="dialog">
	<div class="modal-dialog modal-suppression">
		<div class="modal-content">
			<div class="modal-body">
				<h3>Êtes-vous sûr de vouloir supprimer ce TAF?</h3>
				<div class="field-group">
					<a href="#">OUI</a>
					<a href="#">NON</a>
				</div>
			</div>
		</div>
	</div>
</div>


<?php include 'includes/footer.php' ?>
