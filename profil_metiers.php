<!-- Header -->
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>

<header class="small_header profil_infos">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1>Mes métiers</h1>
			</div>
		</div>
	</div>

</header>

	<!-- PROFIL NAV -->
	<nav class="nav-inscription cf">
		<a href="profil_infos.php" class="col-sm-4 col-xs-12">1. Vos données</a>
		<a href="#" class="active col-sm-4 col-xs-12">2. Vos métiers</a>
		<a href="profil_exp.php" class="col-sm-4 col-xs-12">3. Vos expérience</a>
	</nav>


	<!-- CONTAINER -->
	<div class="container  inscription-container jobs-container">
		<div class="row">
			<div class="col-sm-6 jobs">
				<h3>Vos métiers actifs</h3>
				<table class="table-metiers">
					<thead>
						<tr>
							<th>Actions</th>
							<th>Métiers</th>
						</tr>
					</thead>
					<tbody>
						<!-- METIER 1 -->
						<tr>
							<td>
								<a href="#" class="btn-icon">
								<i class="fa fa-trash"></i>
								Supprimer
								</a>
							</td>
							<td class="metier">
								Commerçant de produits informatiques
							</td>
						</tr>

						<!-- METIER 2 -->
						<tr>
							<td>
								<a href="#" class="btn-icon">
								<i class="fa fa-trash"></i>
								Supprimer
								</a>
							</td>
							<td class="metier">
								Commerçant de produits informatiques
							</td>
						</tr>

						<!-- METIER 3 -->
						<tr>
							<td>
								<a href="#" class="btn-icon">
								<i class="fa fa-trash"></i>
								Supprimer
								</a>
							</td>
							<td class="metier">
								Commerçant de produits informatiques
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-sm-6 jobs-right">
				<form class="jobs-form cf" action="#" method="post">
					<div class="field-group">
						<div class="form-input">
							<label for="job-input">Quel est votre métier ? <span class='light'>(limite de 3 ajouts ! )</span></label>
							<input type="text" name="jobs-input" placeholder="Votre métier" id="nom">
							<div class="tva-icon fa fa-suitcase form-icon"></div>
					</div>
					</div>
					<button type="button" name="button" class="btn-mauve floatright">Ajouter</button>
				</form>
				<section class="not-found">
					<h4>Un métier introuvable?</h4>
					<p>
						Remplissez notre <a href="inscription-new-taf.php">formulaire de demande d’ajout de métier</a>
						aﬁn que l’on puisse l’ajouter sur notre site
					</p>
				</section>
			</div><!-- jobs-right -->
			<div class="nav-btns col-sm-12 cf nav-metiers">
				<button type="button" name="button" class="btn-pages">Confirmer les modifications</button>
			</div>
		</div>
	</div>
 <?php include 'includes/footer.php' ?>
