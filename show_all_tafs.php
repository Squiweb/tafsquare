<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>


<header class="small_header show_all_tafs_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<form class="inscription-form filters_header row" action="#" method="post">
			<div class="field-group col-sm-2 col-sm-offset-2 search input">
				<input type="text" name="name" placeholder="Je recherche">
			</div>
			<div class="field-group col-sm-2">
				<label for="cities" class="localisation">Localisation</label>
				<select name="cities" id="cities">
					<option value="all">Toutes les villes</option>
				</select>
			</div>
			<div class="field-group col-sm-2">
				<label for="domaines">Domaine</label>
				<select name="domaines" id="domaines">
					<option value="all">Tous les domaines</option>
				</select>
			</div>
			<div class="col-sm-2 col-btn">
				<a href="#" class="btn-yellow">Rechercher</a>
			</div>
		</form>
	</div>

</header>


<div class="select_category filters_tafs">
	<div class="container">
		<form class="inscription-form row filters" action="#" method="post">
			<div class="col-md-4 col-sm-offset-2">
				<label for="elements">Elements affichés</label>
				<select class="elements" name="elements" id="elements">
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="30">30</option>
					<option value="40">40</option>
					<option value="50">50</option>
				</select>
			</div>
			<div class="col-md-4">
				<label for="tri">Tier par</label>
				<select class="elements" name="elements" id="elements">
					<option value="recently">Les plus récentes</option>
					<option value="views">Les plus vues</option>
				</select>
			</div>
		</form>
	</div>
</div>



<div class="category_tafs show_all_tafs">

	<div class="container">

		<div class="row">

			<div class="col-md-12 col-centered">

				<ul class="four_up tiles">
					<!-- TAF -->
					<li class="taf middle-time">

						<div class="hoverit">
							<h4>Voir</h4>
						</div>
						<span class="budget">Budget<br>1500€</span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Charpentier</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Charleroi
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 5 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf middle-time">

						<div class="hoverit">
							<h4>Voir</h4>
						</div>
						<span class="budget">Budget<br>1500€</span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Charpentier</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Charleroi
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 5 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf middle-time">

						<div class="hoverit">
							<h4>Voir</h4>
						</div>
						<span class="budget">Budget<br>1500€</span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Charpentier</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Charleroi
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i>du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 5 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf middle-time">

						<div class="hoverit">
							<h4>Voir</h4>
						</div>
						<span class="budget">Budget<br>1500€</span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Charpentier</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Charleroi
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 5 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf middle-time">

						<div class="hoverit">
							<h4>Voir</h4>
						</div>
						<span class="budget">Budget<br>1500€</span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Charpentier</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Charleroi
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 5 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf middle-time">

						<div class="hoverit">
							<h4>Voir</h4>
						</div>
						<span class="budget">Budget<br>1500€</span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Charpentier</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Charleroi
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 5 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf middle-time">

						<div class="hoverit">
							<h4>Voir</h4>
						</div>
						<span class="budget">Budget<br>1500€</span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Charpentier</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Charleroi
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 5 jours</span>
					</li>
					<!-- TAF -->
					<li class="taf middle-time">

						<div class="hoverit">
							<h4>Voir</h4>
						</div>
						<span class="budget">Budget<br>1500€</span>
						<img src="http://www.placehold.it/400x260" alt="exemple">
						<span class="title">Charpentier</span>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
						</p>
						<div class="info-taf">
							<div class="col-md-12">
								<i class="fa fa-map-marker"></i> Charleroi
							</div>
							<div class="col-md-12">
								<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
							</div>
						</div>
						<span class="expire">Expire dans 5 jours</span>
					</li>
				</ul>

			</div>

		</div>

	</div>

</div>


<div class="block-pagination">
	<ul class="pagination">
		<li>
			<a href="#" aria-label="Previous">
				<span aria-hidden="true">&laquo;</span>
			</a>
		</li>
		<li class="active"><a href="#">1</a></li>
		<li><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#">4</a></li>
		<li><a href="#">5</a></li>
		<li>
			<a href="#" aria-label="Next">
				<span aria-hidden="true">&raquo;</span>
			</a>
		</li>
	</ul>
</div>


<?php include 'includes/footer.php' ?>
