<!-- Header -->
<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>
<header class="small_header light_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1>Votre intérêt pour un TAF</h1>
			</div>
		</div>
	</div>

</header>
<!-- CONFIRM CONTAINER -->
<section class="message">
<div class="container">
	<div class="row row_logo">
		<div class="background_logo col-sm-4">
			<img src="img/logo-bg.png" alt="Logo Tafsquare">
		</div>
		<div class="col-sm-8 confirmation">
			<h2>Félicitations !</h2>
			<p>
				Votre intérêt a bien été pris en compte. <br>
        Votre profil sera communiqué à l'organisateur du TAF.
			</p>
			<p class="green bold confirmation_green">
				Vous serez peut-être contacté prochainement par le commanditaire.
				<span class="inline-block">(Délai moyen 3 à 7 jours)</span>
			</p>

			<div class="cf">
				<button type="button" name="button" class="btn-pages">Accueil</button>
			</div>
		</div>
	</div>
</div>
</section>
<?php include 'includes/footer.php' ?>
