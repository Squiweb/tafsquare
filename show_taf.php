<?php include 'includes/head.php' ?>
<?php include 'includes/navbar.php' ?>




<header class="small_header">

	<div class="bg">
		<img src="https://images.unsplash.com/photo-1433840496881-cbd845929862?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=bd85345b7cf94980f2fdf498b9dc53bc">
	</div>

	<div class="container vertical_align">
		<div class="row row-centered">
			<div class="col-md-12 col-centered">
				<h1 class="asterix">Charpentier</h1>
			</div>
		</div>
	</div>

</header>



<div class="inner_taf_info">
	<div class="container">


		<div class="price col-sm-4 col-sm-offset-3">
			<span class="le_prix">1500€ htva</span>
			<div class="col-md-12 price_info">
				<i class="fa fa-map-marker"></i> Charleroi
			</div>
			<div class="col-md-12 price_info">
				<i class="fa fa-clock-o"></i> du 4 janv. au 12 janv. 2016
			</div>
		</div>
	</div>
</div>


<div class="inner_taf show_taf_container">

	<div class="container">

		<div class="row">
			<div class="col-sm-12 top-title">
				<h2>Vos compétences ont des amateurs.</h2>
				<h3>Marquez votre intérêt.</h3>
			</div>
			<div class="col-sm-12 titre">
				<h2>Titre:</h2>
				<p>Cherche charpentier pour ma maison</p>
			</div>

			<div class="col-sm-12 description">
				<h2>Description:</h2>
				<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo- do consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
					Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
				</div>
				<div class="col-sm-12">
					<div class="warning_connection description">
						<p>Attention! Avant de marquer votre intérêt, veuillez vous <a href="#" data-toggle="modal" data-target="#seConnecter">connecter</a> si vous avez déjà un compte Tafsquare ou vous <a href="inscription-tva.php">inscrire</a> si vous êtes un nouvel utilisateur.</p>
					</div>
				</div>
				<form class="inscription-form" action="#" method="post" class="col-sm-12 accept ">

					<div class="form-input col-sm-6 field-group">
						<label for="tva">Numéro de TVA <span class="asterix">*</span></label>
						<input type="text" name="tva" value="" placeholder="(9 ou 10 chiffres)" id="society">
						<div class="tva-icon fa fa-map-marker form-icon"></div>
					</div>
					<div class="form-input col-sm-6 field-group">
						<label for="society">Numéro de gsm <span class="asterix">*</span></label>
						<input type="text" name="gsm" value="" placeholder="Votre numéro de GSM" id="society">
						<div class="tva-icon fa fa-phone form-icon"></div>
					</div>
					<div class="form-input col-sm-6 field-group">
						<label for="society">Numéro de téléphone <span class="asterix">*</span></label>
						<input type="text" name="tva" value="" placeholder="Votre numéro de téléphone" id="society">
						<div class="tva-icon fa fa-phone form-icon"></div>
					</div>

					<div class="form-input input-cgu col-sm-6 field-group">
						<input type="checkbox" name="cgu" id="cgu" value="true">
						<label for="cgu" class="checkbox-label"><span></span>J'ai lu et j'accepte <a href="cgu.php">les conditions générales d'utilisation</a> de ce site internet</label>
					</div>
				</form>
				<div class="col-sm-6 text-left">
					<a href="#" class="btn-mauve">Je suis intéressé</a>
				</div>
				<div class="col-sm-6 text-right">
					<a href="#" class="btn-pages">Retour</a>
				</div>

			</div><!-- ROW -->

		</div><!-- CONTAINER -->

	</div>


	<!-- TRIGGER MODALS -->
	<!-- Modal -->
	<div class="modal fade" id="seConnecter" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content seconnecter">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="modal-title">Se connecter</h3>
				</div>
				<div class="modal-body">
					<div class="form-input field-group">
						<label for="email">Email :</label>
						<input type="text" name="email" value="" placeholder="Entrez votre e-mail" id="email">
						<label for="mdp">Mot de passe :</label>
						<input type="text" name="mdp" value="" placeholder="Entrez votre mot de passe" id="mdp">
						<input type="checkbox" name="remember" value="true" id="remember">
            <label for="remember" class="checkbox-label"><span></span>Se rappeller de moi</label>
						<a class="forgetpw" data-toggle="modal" data-target="#forgetPw" href="#">Mot de passe oublié</a>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-pages" data-dismiss="modal">Se connecter</button>
				</div>
			</div>
		</div>
	</div>


	<!-- Modal Forget Password -->
	<div class="modal fade" id="forgetPw" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content forgetpw">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="modal-title">Mot de passe oublié : </h3>
				</div>
				<div class="modal-body">
					<div class="form-input field-group">
						<label for="email">Email ou nom d'utilisateur<span class="asterix">*</span></label>
						<input type="text" name="email" value="" placeholder="Entrez votre e-mail ou votre nom d'utilisateur" id="email">
						<div class="text-right">
							<button type="button" class="btn-pages" data-dismiss="modal">Envoyer</button>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<p>Vérifiez vos derniers mails reçus pour réinitialiser votre mot de passe</p>
				</div>
			</div>
		</div>
	</div>


	<?php include 'includes/footer.php' ?>
