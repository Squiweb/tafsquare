<?php
include 'includes/head.php';
include 'includes/navbar.php';
include 'includes/cookies.php';
?>

<div class="container sg-container">
  <div class="row">
    <section class="title">
      <div class="col-sm-12 bg-mauve">
        <a href="#" class="logo"><img src="img/logo.png" alt="TafSquare"/></a>
      </div>
    </section>
    <!--TYPOGRAPHIE-->
    <section class="typographie">
      <div class="col-sm-12">
        <h1 class="sg-title">Typographie</h1>
      </div>
      <div class="col-sm-4">
        <h1>Title 1</h1>
        <h2>Title 2</h2>
        <h3>Title 3</h3>
        <h4>Title 4</h4>
        <h5>Title 5</h5>
      </div>
      <div class="col-sm-4">
        <h1>Body Text</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </div>
      <div class="col-sm-4">

      </div>
    </section>

    <!-- BUTTONS -->
    <section class="typographie">
      <div class="col-sm-12">
        <h1 class="sg-title">Buttons</h1>
      </div>
      <div class="col-sm-4 sg-btns">
        <button type="button" name="button" class="btn-single">Homepage</button>
        <button type="button" name="button" class="btn-classic">Classic</button>
        <button type="button" name="button" class="btn-rouge">Rouge</button>
        <button type="button" name="button" class="btn-vert">Vert</button>
        <button type="button" name="button" class="btn-mauve">Mauve</button>
        <button type="button" name="button" class="btn-supprimer">Supprimer</button>
      </div>
      <div class="col-sm-4">

      </div>
      <div class="col-sm-4">

      </div>
    </section>

    <!-- INPUTS -->
    <section class="typographie">
      <div class="col-sm-12">
        <h1 class="sg-title">Inputs</h1>
      </div>
      <div class="col-sm-6">
        <form class="inscription-form inscription-container" action="#" method="post">
          <div class="form-input field-group">
            <label for="tva-input">Entrez votre numéro de TVA ou numéro d'entreprise<span class="asterix">*</span></label>
            <input type="text" name="tva" value="" placeholder="(9 ou 10 chiffres)" id="tva-input">
            <div class="tva-icon fa fa-map-marker sg-icon form-icon"><!--<span class="tooltip">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span>--></div>
          </div>
        </form>
      </div>
      <div class="col-sm-4">

        <div class="dropdown">
          <button class="btn-classic dropdown-toggle-select dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Dropdown
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu dropdown-menu-select" aria-labelledby="dropdownMenu1">
            <li><a href="#">Menu 1</a></li>
            <li><a href="#">Menu 2</a></li>
            <li><a href="#">Menu 3</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Menu 4</a></li>
          </ul>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="alert alert-danger">
          Ce champs est requis.
        </div>
        <div class="alert alert-success">
          Ce champs est requis.
        </div>
      </div>
    </section>

  </div><!--row-->
</div><!--container-->

<?php include 'includes/footer.php' ?>
